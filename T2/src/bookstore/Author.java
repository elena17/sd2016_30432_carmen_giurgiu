package bookstore;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Giurgiu Carmen
 */
public class Author {
    
    public static void insertAuthor(int id, String firstName, String lastName){
            String sql = " insert into author (id, firstName, lastname) values(" + id +"," + firstName + ","+ lastName +")" ;
           
		try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
        }
    
    public static void updateAuthor(int id, String firstName, String lastName){
            String sql = "update author set firstName = "+ firstName +", lastName = "+ lastName +" where id = "+id;
            try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
    }
    
    public static void deleteAuthor(int id){
            String sql = "delete from author where id = "+id;
            try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
    }   
	
}
