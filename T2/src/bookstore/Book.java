package bookstore;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Giurgiu Carmen
 */
public class Book {
     static String firstName=null, lastName=null, ISBN=null, title=null;
     static int price = 0;
    
    public static void insertBook(int id, int idAuthor, String name, String ISBN, int price)
	{
		String sql = " insert into book (id, idAuthor, title, ISBN, price) values(" + id +"," + idAuthor + "," + name +","+ISBN+","+price+")";
		try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
        }
    
    
    public static void updateBook(int id, String title, String ISBN, int price){
            String sql = "update book set title = "+ title +", ISBN = "+ ISBN +", price = "+ price +" where id = "+id;
            try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
    }
    
    public static void deleteBook(int id){
            String sql = "delete from book where id = "+id;
            try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
    }
    
    public static ResultSet findBook(String firstName, String lastName)
	{
		String sql = "select firstName, lastName, title, ISBN, price from author, book  where firstName ="+firstName+"and lastName="+lastName+"and author.id = book.idAuthor";
		try {
			return Database.getInstance().executeQuery(sql);
                }catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
       
        public static void main(String [] args){
            Book b = new Book();             
            ArrayList<String> arr = new ArrayList(); 
            String st = null;
            
            ResultSet rs = b.findBook("'Eco'", "'Umberto'");
            
            try {                 
                   while(rs.next()){                      
                        arr.add(rs.getString("firstName"));
                        arr.add(rs.getString("lastName"));
                        arr.add(rs.getString("Title"));
                        arr.add(rs.getString("ISBN"));
                        //arr.add(rs.getString(Integer.toString(price)));
                    }
                   
                    st = String.join(" ", arr);
                    System.out.println(st);
                    
                }
            catch (Exception ae) {
                ae.printStackTrace();
            }
            
          
    } 
        
}
