/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author vaio
 */
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

//Service Endpoint Interface
@WebService
@SOAPBinding(style = Style.RPC)
public interface  UserManagerInterface {

    @WebMethod
    public User login(String username, String password);

    @WebMethod
    public String addUser(int id, String name, String username, String password, String type);

    @WebMethod
    public String updateUser(int oldId, int id, String name, String username, String password, String type);

    @WebMethod
    public Object[] getUsers();    
    
    
    @WebMethod
    public Object[] getDoctors();

    @WebMethod
    public User getUser(int id);

   @WebMethod
    public String deleteUser(int id);
}