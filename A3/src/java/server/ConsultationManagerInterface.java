/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 *
 * @author vaio
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface  ConsultationManagerInterface {

    @WebMethod
    public Object[] getConsultationsByPatientPNC(long PNC);

    @WebMethod
    public String addConsultation(int id, String date, long patientPNC, int doctorID, String text);

    @WebMethod
    public String update(int oldID, int id, String date, long patientPNC, int doctorID, String text);

    @WebMethod
    public Object[] getConsultations();

    @WebMethod
    public Consultation getConsultation(int ID);


    @WebMethod
    String deleteConsultation(int id);

    @WebMethod
    String updateDoctorConsultation(int id, String text);
}