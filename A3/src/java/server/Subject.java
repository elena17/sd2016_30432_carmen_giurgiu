/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.util.ArrayList;
import client.Observer;


/**
 *
 * @author vaio
 */
public abstract class Subject {

    private ArrayList<Observer> ObserverList=new ArrayList<Observer>();

    public ArrayList<Observer> getObserverList()
    {
        return ObserverList;
    }

    public String register(Observer Observer)
    {
        ObserverList.add(Observer);
        return  "registered";
    }

    public void unregister(Observer Observer)
    {
        ObserverList.remove(Observer);
    }

    public String notifyObservers()
    {
        for (Observer Observer:ObserverList)
            Observer.update();
        return  "Observers notified";
    }
}
