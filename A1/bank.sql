-- MySQL dump 10.13  Distrib 5.5.9, for Win32 (x86)
--
-- Host: localhost    Database: bank
-- ------------------------------------------------------
-- Server version	5.1.35-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `bank`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `bank` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bank`;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `idaccount` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idclient` int(10) unsigned NOT NULL,
  `idtype` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned DEFAULT '0',
  `creationDate` datetime NOT NULL,
  PRIMARY KEY (`idaccount`),
  UNIQUE KEY `id_UNIQUE` (`idaccount`),
  KEY `FK_ACCOUNTS_CLIENTS` (`idclient`),
  KEY `FK_ACCOUNTS_TYPES` (`idtype`),
  CONSTRAINT `FK_ACCOUNTS_CLIENTS` FOREIGN KEY (`idclient`) REFERENCES `client` (`idclient`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ACCOUNTS_TYPES` FOREIGN KEY (`idtype`) REFERENCES `accounttype` (`idtype`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,1,2,3500,'2016-03-23 19:58:20'),(2,1,1,2500,'2015-03-24 11:04:03'),(3,1,1,7097,'2016-03-24 11:08:11'),(4,3,1,0,'2014-03-25 13:19:34');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounttypes`
--

DROP TABLE IF EXISTS `accounttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounttype` (
  `idtype` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(15) NOT NULL,
  PRIMARY KEY (`idtype`),
  UNIQUE KEY `id_UNIQUE` (`idtype`),
  UNIQUE KEY `type_UNIQUE` (`idtype`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounttypes`
--

LOCK TABLES `accounttype` WRITE;
/*!40000 ALTER TABLE `accounttype` DISABLE KEYS */;
INSERT INTO `accounttype` VALUES (2,'Current'),(1,'Savings');
/*!40000 ALTER TABLE `accounttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `idclient` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `cardnumber` varchar(10) NOT NULL,
  `cnp` varchar(13) NOT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idclient`),
  UNIQUE KEY `id_UNIQUE` (`idclient`),
  UNIQUE KEY `identityCardNumber_UNIQUE` (`cardnumber`),
  UNIQUE KEY `personalNumericNumber_UNIQUE` (`cnp`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'Pooescu Ionut','KX426031','1910504125785','Cluj-Napoca'),(2,'Razvan ','KX426032','1910404125786','Magaziei 13'),(3,'Utilities Company','KX123456','1750312178652','-');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `idemployee` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(25) NOT NULL,
  `isadmin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`idemployee`),
  UNIQUE KEY `id_UNIQUE` (`idemployee`),  
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'Giurgiu Carmen','elena','pass',1),(2,'Giurgiu Carmen','elena1','pass1',0);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `idtransaction` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idemployee` int(10) unsigned NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `idfromaccount` int(10) unsigned NOT NULL,
  `idtoaccount` int(10) unsigned DEFAULT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`idtransaction`),
  UNIQUE KEY `id_UNIQUE` (`idtransaction`),
  KEY `FK_TRANSACTIONS_EMPLOYEES` (`idemployee`),
  KEY `FK_TRANSACTIONS_ACCOUNTS1` (`idfromaccount`),
  KEY `FK_TRANSACTIONS_ACCOUNT2` (`idtoaccount`),
  CONSTRAINT `FK_TRANSACTIONS_ACCOUNTS1` FOREIGN KEY (`idfromaccount`) REFERENCES `account` (`idaccount`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TRANSACTIONS_EMPLOYEES` FOREIGN KEY (`idemployee`) REFERENCES `employee` (`idemployee`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;



