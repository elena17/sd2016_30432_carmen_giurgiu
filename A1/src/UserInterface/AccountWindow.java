package UserInterface;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.util.ArrayList;

import DomainLayer.Account;
import DomainLayer.Client;
import java.awt.Color;

public class AccountWindow {
	private JFrame frame;
	private Client client;
        private Account account;
	private MainWindow mainWindow;
	private JPanel panel2;
	private JTable table;
	private ArrayList<Account> accountList;
	private AccountWindow thisWindow;   
	private class FrameWindowListener implements WindowListener
	{
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			mainWindow.makeVisible();
		}

		public void windowDeactivated(WindowEvent arg0) {
		}
		public void windowDeiconified(WindowEvent arg0) {
		}
		public void windowIconified(WindowEvent arg0) {
		}
		public void windowOpened(WindowEvent arg0) {
		}
		
	}
	
	
	
	private class CustomTableModule extends DefaultTableModel
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 6291865013403466375L;
		private Object[][] data;
		String[] columns = {"Id","Amount", "Type", "Creation Date"};;
		public CustomTableModule(Object[][] data)
		{
			this.data = data;
		}
		
                @Override
		public int getRowCount() { 
                    return data == null ? 0 : data.length; 
                }
                
                @Override
                public int getColumnCount() { 
                    return data == null ? 0 : 4;
                }
                
		
                @Override
		public boolean isCellEditable(int row, int col){
			return col == 2;
		}
                
		
                @Override
		public Object getValueAt(int row, int col){
		    return data[row][col];
		}
		
                
                @Override
		 public String getColumnName(int col){
		        return columns[col];
		 }
                 
                 
                @Override
		public void setValueAt(Object value, int row, int col){
		   data[row][col] = value;
		   fireTableCellUpdated(row, col);
		}
                
	}
        
        
	
	private class TableMouseListener implements MouseListener
	{

		public void mouseClicked(MouseEvent arg0) {
			if(arg0.getButton() == MouseEvent.BUTTON3)
			{
				final int accountIndex = table.rowAtPoint(arg0.getPoint());                               
				JPopupMenu popupMenu = new JPopupMenu();
				JMenuItem delete = new JMenuItem("Delete Account");
				delete.addActionListener(new ActionListener()
				{
                                    
					public void actionPerformed(ActionEvent arg0) {
                                                
						String s = "Are you sure you want to delete account:";
						s += accountList.get(accountIndex).getIdAccount();                                                
						if(JOptionPane.showConfirmDialog(table, s) == JOptionPane.OK_OPTION)
						{
							Account account = accountList.get(accountIndex);
							account.deleteAccount();
							displayAccounts();
						}
					}
					
				});
				popupMenu.add(delete);
				JMenuItem deposit = new JMenuItem("Deposit money");
				deposit.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						WithdrawDepositWindow.WithdrawDepositWindowType type = WithdrawDepositWindow.WithdrawDepositWindowType.DEPOSIT;
						new WithdrawDepositWindow(thisWindow, accountList.get(accountIndex), type);
						frame.setVisible(false);
					}
					
				});
				popupMenu.add(deposit);
				JMenuItem withdraw = new JMenuItem("Withdraw money");
				withdraw.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						WithdrawDepositWindow.WithdrawDepositWindowType type = WithdrawDepositWindow.WithdrawDepositWindowType.WITHDRAW;
						new WithdrawDepositWindow(thisWindow, accountList.get(accountIndex), type);
						frame.setVisible(false);
					}
					
				});
				popupMenu.add(withdraw);
				JMenuItem paybills = new JMenuItem("Pay bills");
				paybills.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						WithdrawDepositWindow.WithdrawDepositWindowType type = WithdrawDepositWindow.WithdrawDepositWindowType.PAY_BILLS;
						new WithdrawDepositWindow(thisWindow, accountList.get(accountIndex), type);
						frame.setVisible(false);
					}
					
				});
				popupMenu.add(paybills);
				JMenuItem transfer = new JMenuItem("Transfer money");
				transfer.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						new TransferWindow(thisWindow, accountList.get(accountIndex));
						frame.setVisible(false);
					}
					
				});
				popupMenu.add(transfer);
				popupMenu.show(table, table.getX() + arg0.getX(), table.getY() + arg0.getY());
			}
		}

		public void mouseEntered(MouseEvent arg0) {
		}

		public void mouseExited(MouseEvent arg0) {
		}

		public void mousePressed(MouseEvent arg0) {
		}

		public void mouseReleased(MouseEvent arg0) {
		}
		
	}
	
	private class TableKeyListener implements KeyListener
	{
		public void keyPressed(KeyEvent arg0) {
		}

		public void keyReleased(KeyEvent arg0) {
		}

		public void keyTyped(KeyEvent arg0) {	
			if(arg0.getKeyChar() == KeyEvent.VK_ENTER)
			{
				int row = table.getSelectedRow();
                                
				row--;
				if(row == -1)
					row = table.getRowCount() - 1;
				Account account = accountList.get(row);
				String type = (String) table.getValueAt(row, 2);                                
				account.setType(type);
				account.updateAccount();
			}
		}
	}
	
	public void displayAccounts()
	{
		for(Component c : panel2.getComponents())                    
			panel2.remove(c);
		panel2.add(new JLabel("                                                    "), BorderLayout.NORTH);
                panel2.setBackground(Color.cyan);
          
                client.readAccounts();
		ArrayList<Account> accounts = client.getAccount();
                accountList = accounts;
               
                
                //ArrayList<Account> accounts = Account.readAllAccounts();
                //accountList = accounts;              
                
              
                
		String[][] data = new String[accounts.size()][];
		int i = 0;
		for(Account a : accounts)
		{
			data[i] = new String[4];
			data[i][0] = Integer.toString(a.getIdAccount());
			data[i][1] = Integer.toString(a.getAmount());
			data[i][2] = a.getType();
			data[i][3] = a.getCreationDate();
			i++;
		}
		table = new JTable(new CustomTableModule(data));
		table.addKeyListener(new TableKeyListener());
		table.addMouseListener(new TableMouseListener());
		TableColumn typeColumn = table.getColumnModel().getColumn(2);
		JComboBox<String> comboBox = new JComboBox<String>();
		ArrayList<String> types = Account.getPossibleAccountTypes();
		for(String s : types)
			comboBox.addItem(s);
		typeColumn.setCellEditor(new DefaultCellEditor(comboBox));
		panel2.add(new JScrollPane(table), BorderLayout.CENTER);
		frame.validate();
	}
        
        
        
        
        public AccountWindow(Client client, MainWindow mainWindow)
	{
		this.mainWindow = mainWindow;
		thisWindow = this;
		frame = new JFrame();
		frame.setSize(800, 500);
                frame.setLocation(400, 80);
		frame.addWindowListener(new FrameWindowListener());
                frame.setTitle("Account Window");
		JMenuBar menuBar = new JMenuBar();
		this.client = client;
		JMenuItem menu = new JMenuItem("Add Account");	
                menu.setBackground(Color.cyan);
                menu.setBorder(null);
		menuBar.add(menu);
                menuBar.setBorder(null);
                panel2 = new JPanel();
		panel2.setLayout(new BorderLayout());
		final AccountWindow window = this;
               
                
                
		menu.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) 
			{
				frame.setVisible(false);                                
				new AddAccountWindow(window, client.getIdClient());
                                
			}
		});
               
                
		displayAccounts();
		JPanel panel1 = new JPanel();
		panel1.add(menuBar);
		JPanel panel3 = new JPanel();
		panel3.setLayout(new BorderLayout());
		panel3.add(panel1, BorderLayout.NORTH);
		panel3.add(panel2, BorderLayout.CENTER);
                
                panel1.setBackground(Color.cyan);
		frame.add(panel3);
		frame.validate();
		frame.setVisible(true);
	}
	
	public void makeVisible()
	{
		frame.setVisible(true);
	}
	
}
