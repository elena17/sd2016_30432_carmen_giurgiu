package UserInterface;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.*;

import DomainLayer.Account;
import java.awt.Color;

public class AddAccountWindow {
	private JComboBox<String> typesComboBox;
	private JFrame frame;
	private AccountWindow window;
	private int idClient;
	private JTextField amountTextField;
        private JPanel panel1, panel2, panel3;
        
	private class FrameWindowListener implements WindowListener{
            @Override
		public void windowActivated(WindowEvent arg0) {
		}

                @Override
		public void windowClosed(WindowEvent arg0) {
		}

                @Override
		public void windowClosing(WindowEvent arg0) {	
			window.makeVisible();
		}

                @Override
		public void windowDeactivated(WindowEvent arg0) {
		}
                @Override
		public void windowDeiconified(WindowEvent arg0) {
		}
                @Override
		public void windowIconified(WindowEvent arg0) {
		}
                @Override
		public void windowOpened(WindowEvent arg0) {
		}
		
	}
	
	private class AddAccountButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			try{
				
			int value  = Integer.parseInt(amountTextField.getText());
			String type = typesComboBox.getItemAt(typesComboBox.getSelectedIndex());
			Account.createAccount(idClient, value, type);
			window.displayAccounts();
			window.makeVisible();
			frame.dispose();
			
			} catch (Exception e1)
			{
				JOptionPane.showMessageDialog(frame, "Amount entered is not a valid number", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}
	public AddAccountWindow(AccountWindow window, int clientId)
	{
		frame = new JFrame();
                frame.setSize(600, 200);
                frame.setLocation(400, 80);                
		this.window = window;
		this.idClient = clientId;
		frame.addWindowListener(new FrameWindowListener());
		
		ArrayList<String> types = Account.getPossibleAccountTypes();
		typesComboBox = new JComboBox<String>();
		for(String s : types)
			typesComboBox.addItem(s);
		typesComboBox.setSelectedIndex(0);
		panel1 = new JPanel();
		panel1.setLayout(new GridLayout(2,2 ));
		panel1.add(new JLabel("Amount:"));
		amountTextField = new JTextField();
		amountTextField.setText("0");
		panel1.add(amountTextField);
		panel2  = new JPanel();
		panel3 = new JPanel();
		JButton button = new JButton("Add Account");
		button.addActionListener(new AddAccountButtonListener());
		panel2.add(button);
		panel3.setLayout(new BorderLayout());
		panel3.add(panel1, BorderLayout.NORTH);
		panel3.add(panel2, BorderLayout.EAST);
		panel1.add(new JLabel("Account Type:"));
		panel1.add(typesComboBox);
		
                panel1.setBackground(Color.cyan);
                panel2.setBackground(Color.cyan);
                panel3.setBackground(Color.cyan);
                
		frame.add(panel3);
		frame.validate();
		frame.setVisible(true);
	}
	
}
