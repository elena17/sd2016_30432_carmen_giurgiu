package UserInterface;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.*;

import DomainLayer.Client;
import DomainLayer.Employee;
import java.awt.Color;
import javax.swing.text.TableView.TableRow;


public class MainWindow {
	private JFrame frame;
	private JPanel panel1, panel2, panel3;
	private ArrayList<Client> clients;
	private JTable table;
	private JPopupMenu popupMenu;
	private MainWindow mainWindow;
	
	private class TableMouseListener implements MouseListener
	{

		public void mouseClicked(MouseEvent e) {
			if(e.getButton() == MouseEvent.BUTTON3)  //if right click
			{
				final int clientIndex = table.rowAtPoint(e.getPoint());                          
                                                             
				popupMenu = new JPopupMenu();                              
                                
                                
                                
                                JMenuItem delete = new JMenuItem("Delete Client");
				delete.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						
						String s = clients.get(clientIndex).getName() + "?";
						if(JOptionPane.showConfirmDialog(table, s) == JOptionPane.OK_OPTION)
						{							
                                                        Client client = clients.get(clientIndex);
                                                        client.delete();							
							displayClients();
						}
					}
					
				});                       
                                
                                                              
                                
				JMenuItem manageAccounts = new JMenuItem("Manage Accounts");                               
				manageAccounts.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						Client client = clients.get(clientIndex);                                                                                                
						frame.setVisible(false);
						new AccountWindow(client, mainWindow);
					}
					
				});
                                popupMenu.add(delete);                                
				popupMenu.add(manageAccounts);
				popupMenu.show(table, table.getX() + e.getX(), table.getY() + e.getY());
			}
		}
                
                

               
                @Override
		public void mouseEntered(MouseEvent arg0) {
		}

                @Override
		public void mouseExited(MouseEvent arg0) {
		}

                @Override
		public void mousePressed(MouseEvent arg0) {
		}

                @Override
		public void mouseReleased(MouseEvent arg0) {
		}
		
	}
	
        
        
	private class TableKeyListener implements KeyListener
	{
                @Override                
                public void keyPressed(KeyEvent arg0) {
		}

                @Override
		public void keyReleased(KeyEvent arg0) {
		}

                @Override
		public void keyTyped(KeyEvent arg0) {	
			if(arg0.getKeyChar() == KeyEvent.VK_ENTER)
			{
				int row = table.getSelectedRow();
				row--;
				if(row == -1)
					row = table.getRowCount() - 1;
				Client client = clients.get(row);
                                
				String name = (String) table.getValueAt(row, 0);
				String identityCardNumber = (String) table.getValueAt(row, 1);
				String personalNumericCode = (String) table.getValueAt(row, 2);
				String address = (String) table.getValueAt(row, 0);
				if(name.isEmpty())
				{
					JOptionPane.showMessageDialog(frame, "Name cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				if(!name.matches("[ ([A-Z][a-z]+)]+"))
				{
					JOptionPane.showMessageDialog(frame, "Name must contain only letters and spaces", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				if(!identityCardNumber.matches("[A-Z][A-Z][0-9]{6}"))
				{
					JOptionPane.showMessageDialog(frame, "Idendtity card number invalid", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				if(!personalNumericCode.matches("[1-6][0-9]{12}"))
				{
					JOptionPane.showMessageDialog(frame, "Personal numeric code must be composed of 13 digits", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				if(address.isEmpty())
				{
					JOptionPane.showMessageDialog(frame, "Address cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				client.setName(name);
				client.setIdentityCardNumber(identityCardNumber);
				client.setCnp(personalNumericCode);
				client.setAddress(address);
				
				client.update();
			}
		}
	}
	
	public MainWindow()
	{
		mainWindow = this;                
		frame = new JFrame();
		frame.setSize(800, 600);
                frame.setLocation(400, 80);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Logged in as: " + Employee.getLoggedInEmployee().getName());
                
		JMenuBar menuBar = new JMenuBar();                
		JMenuItem menu = new JMenuItem("Add Client");
                menu.setBackground(Color.cyan);
		menuBar.add(menu);
		final MainWindow window = this;
		menu.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) 
			{
				new AddClientWindow(window);
			}
		});                
                               
		
                
                
		JMenuItem logoutMenuItem = new JMenuItem("Log out");
                logoutMenuItem.setBackground(Color.cyan);
		logoutMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) 
			{
				Employee.setLoggedInEmployee(null);
				new LoginWindow();
				frame.dispose();
			}
		});
		menuBar.add(logoutMenuItem);
                
		
                menuBar.setBorder(null);
                
		panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		panel1.add(menuBar, BorderLayout.NORTH);
                
		panel2 = new JPanel();
		panel2.setLayout(new BorderLayout());
	
		displayClients();
		
		
                panel3 = new JPanel();
		panel3.setLayout(new BorderLayout());
		panel3.add(panel1, BorderLayout.NORTH);
		panel3.add(panel2, BorderLayout.CENTER);
		
		panel1.setBackground(Color.cyan);
                panel2.setBackground(Color.cyan);
                panel3.setBackground(Color.cyan);
                        
		frame.add(panel3);
		frame.validate();
		frame.setVisible(true);
	}

	public void makeVisible()
	{
		frame.setVisible(true);
	}
	
	public void displayClients()
	{
		for(Component comp : panel2.getComponents())
		{
			panel2.remove(comp);
		}
		panel2.add(new JLabel("                                                                  "), BorderLayout.NORTH);
		String[] columns = {"Id" ,"Name", "Identity Card Number", "Personal Numerical Code", "Address"};
		String[][] data;
		clients = Client.readAllClients();
		data = new String[clients.size()][];
		for(int i = 0; i < data.length; i++)
		{
			Client client = clients.get(i);
			data[i] = new String[5];                         
                        data[i][0] = ""+client.getIdClient();
                        data[i][1] = client.getName();
			data[i][2] = client.getCardNumber();
			data[i][3] = client.getCnp();
			data[i][4] = client.getAddress();
		}
		table = new JTable(data, columns);
		table.addMouseListener(new TableMouseListener());
		table.addKeyListener(new TableKeyListener());                
		panel2.add(new JScrollPane(table), BorderLayout.CENTER);                
		frame.validate();
	}
}
