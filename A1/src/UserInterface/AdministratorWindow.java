package UserInterface;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import DomainLayer.Employee;
import java.awt.Color;

public class AdministratorWindow 
{
	private JFrame frame;
	private ArrayList<Employee> employeeList;
	private JScrollPane scrollPane;
	private JTable table;
	private JPanel panel1, panel2, panel3;
	private JPopupMenu popupMenu = null;
	private AdministratorWindow thisWindow;
	
	private class TableMouseListener implements MouseListener
	{

		public void mouseClicked(MouseEvent e) {
			if(e.getButton() == MouseEvent.BUTTON3)  //on right click
			{
				int employeeIndex = table.rowAtPoint(e.getPoint());
				popupMenu = new JPopupMenu();
                                
				JMenuItem delete = new JMenuItem("Delete Employee");
				delete.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) {
						String s = "Do you want to delete employee: ";
						s += employeeList.get(employeeIndex).getName() + "?";
						if(JOptionPane.showConfirmDialog(table, s) == JOptionPane.OK_OPTION)
						{
							Employee employee = employeeList.get(employeeIndex);
							employee.delete();
							displayEmployees();
						}
					}
					
				});
                                
				JMenuItem report = new JMenuItem("Generate Report");
				report.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						new ReportWindow(employeeList.get(employeeIndex), thisWindow);
					}
					
				});
				popupMenu.add(delete);
				popupMenu.add(report);
				popupMenu.show(table, table.getX() + e.getX(), table.getY() + e.getY());
			}
		}

                @Override
		public void mouseEntered(MouseEvent e) {
		}

                @Override
		public void mouseExited(MouseEvent e) {
		}

                @Override
		public void mousePressed(MouseEvent e) {
		}

                @Override
		public void mouseReleased(MouseEvent e) {
		}
		
	}
	
	private class TableKeyListener implements KeyListener
	{
		public void keyPressed(KeyEvent arg0) {
		}

		public void keyReleased(KeyEvent arg0) {
		}

		public void keyTyped(KeyEvent arg0) {	
			if(arg0.getKeyChar() == KeyEvent.VK_ENTER)
			{
				int row = table.getSelectedRow();
				row--;
				if(row == -1)
					row = table.getRowCount() - 1;
				Employee emp = employeeList.get(row);
				boolean bool = (Boolean) table.getValueAt(row, 3);
				String name = (String) table.getValueAt(row, 0);
				String username = (String) table.getValueAt(row, 1);
				String password = (String) table.getValueAt(row, 2);
				emp.setAdministrator(bool);
				emp.setName(name);
				emp.setUsername(username);
				emp.setPassword(password);
				emp.update();
			}
		}
	}
	
	private class CustomTableModule extends DefaultTableModel	{
		
		private static final long serialVersionUID = 6291865013403466375L;
		private Object[][] data;
		String[] columns = {"Employee name", "Employee username", "Employee password", "Administrator Rights"};
		public CustomTableModule(Object[][] data)
		{
			this.data = data;
		}
		
                @Override
		public int getRowCount() { return data == null ? 0 : data.length; }
                @Override
                public int getColumnCount() { return data == null ? 0 : 4;}
	    
                @Override
		public Class getColumnClass(int column)
		{
			if(column == 3)
				return Boolean.class;
			else
				return String.class;
		}
		
                @Override
		public boolean isCellEditable(int row, int col)
		{
			return true;
		}
		
		public Object getValueAt(int row, int col) 
		{
		    return data[row][col];
		}
		
		 public String getColumnName(int col)
		 {
		        return columns[col];
		 }
		public void setValueAt(Object value, int row, int col)
		{
		   data[row][col] = value;
		   fireTableCellUpdated(row, col);
		}
	}
	
	public AdministratorWindow()
	{
		thisWindow = this;
		frame = new JFrame();
		frame.setTitle("Logged in as: " + Employee.getLoggedInEmployee().getName());
		frame.setSize(800, 500);
                frame.setLocation(400, 80);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
		JMenuBar menuBar = new JMenuBar();
		JMenuItem menu = new JMenuItem("Add Employee");	
                menu.setBackground(Color.cyan);
		menuBar.add(menu);
                
		final AdministratorWindow window = this;
                
		menu.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) 
			{
				new AddEmployeeWindow(window);
			}
		});
                
                
		JMenuItem logoutMenuItem = new JMenuItem("Log out");
                logoutMenuItem.setBackground(Color.cyan);
		logoutMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) 
			{
				Employee.setLoggedInEmployee(null);
				new LoginWindow();
				frame.dispose();
			}
		});
		menuBar.add(logoutMenuItem);
                menuBar.setBorder(null);
                
		panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		panel1.add(menuBar, BorderLayout.NORTH);
                
		panel2 = new JPanel();
		panel2.setLayout(new BorderLayout());
	
		displayEmployees();
		
		panel3 = new JPanel();
		panel3.setLayout(new BorderLayout());
		panel3.add(panel1, BorderLayout.NORTH);
		panel3.add(panel2, BorderLayout.CENTER);
		
		panel1.setBackground(Color.cyan);
                panel2.setBackground(Color.cyan);
                panel3.setBackground(Color.cyan);
                
		frame.add(panel3);
		frame.validate();
		frame.setVisible(true);
                
                
	}
	
	public void displayEmployees()
	{
		for(Component comp : panel2.getComponents())
		{
			panel2.remove(comp);
		}
		panel2.add(new JLabel("                                  "), BorderLayout.NORTH);
		employeeList = Employee.getAllEmployees();
		Object[][] tableData = new Object[employeeList.size()][];
		for(int i = 0; i < employeeList.size(); i++)
		{
			Employee employee = employeeList.get(i);
			tableData[i] = new Object[4];
			tableData[i][0] = employee.getName();
			tableData[i][1] = employee.getUsername();
			tableData[i][2] = employee.getPassword();
			tableData[i][3] = employee.isAdmin();
		}
		
		table = new JTable(new CustomTableModule(tableData));
		table.addKeyListener(new TableKeyListener());
		scrollPane = new JScrollPane(table);
		table.addMouseListener(new TableMouseListener());
		panel2.add(scrollPane, BorderLayout.CENTER);
		frame.validate();
	}
	
	public void makeVisible()
	{
		frame.setVisible(true);
	}
}
