package UserInterface;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import DomainLayer.Account;
import DomainLayer.Client;
import DomainLayer.Employee;
import DomainLayer.Transaction;
import java.awt.Color;

public class TransferWindow {
	private JFrame frame;
	private AccountWindow window;
	private JSpinner spinner, accountSpinner;
	private Account account;
	private ArrayList<Account> accounts;
	private JTable table;
	private JPanel panel2;
	private class FrameWindowListener implements WindowListener
	{
                @Override
		public void windowActivated(WindowEvent arg0) {
		}
                @Override
		public void windowClosed(WindowEvent arg0) {
		}
                @Override
		public void windowClosing(WindowEvent arg0) {	
			window.makeVisible();
		}
                @Override
		public void windowDeactivated(WindowEvent arg0) {
		}
                @Override
		public void windowDeiconified(WindowEvent arg0) {
		}
                @Override
		public void windowIconified(WindowEvent arg0) {
		}
                @Override
		public void windowOpened(WindowEvent arg0) {
		}
		
	}
        
        
	
	private class ButtonActionListener implements ActionListener 
	{

		public void actionPerformed(ActionEvent arg0) {
			int id = (int) accountSpinner.getValue();
			int amount = (int) spinner.getValue();
			if(amount == 0)
				return;
			Account foreignAccount = null;
			for(Account a : accounts)
				if(a.getIdAccount() == id)
				{
					foreignAccount = a;
					break;
				}
			if(foreignAccount == null)
			{
				JOptionPane.showMessageDialog(frame, "Target account is not valid", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			foreignAccount.setAmmount(foreignAccount.getAmount() + amount);
			foreignAccount.updateAccount();
			account.setAmmount(account.getAmount() - amount);
			account.updateAccount();
			Transaction.create(Employee.getLoggedInEmployee().getIdEmployee(), amount, account.getIdAccount(), id);
			window.displayAccounts();
			window.makeVisible();
			frame.dispose();
		}
		
	}
	public TransferWindow(AccountWindow window, final Account account)
	{
		this.window = window;
		this.account = account;
		frame = new JFrame();
		frame.setSize(800, 600);
                frame.setLocation(400, 80);
		frame.setVisible(true);
		frame.addWindowListener(new FrameWindowListener());
		JPanel panel1 = new JPanel(new GridLayout(2, 2));
		panel1.add(new JLabel("Amount:"));
		SpinnerNumberModel model = new SpinnerNumberModel();
		model.setMinimum(0);
		model.setMaximum(account.getAmount());
		spinner = new JSpinner(model);
		panel1.add(spinner);
		panel1.add(new JLabel("Target account id:"));
		model = new SpinnerNumberModel();
		model.setMinimum(0);
		accountSpinner = new JSpinner(model);
		panel1.add(accountSpinner);
		JPanel panel3 = new JPanel(new BorderLayout());
		panel2 = new JPanel(new BorderLayout());
		loadAccounts();
		panel3.add(panel1, BorderLayout.NORTH);
		panel3.add(panel2, BorderLayout.CENTER);
		JButton button = new JButton("Perform Transfer");
		button.addActionListener(new ButtonActionListener());
		panel3.add(button, BorderLayout.PAGE_END);
                panel1.setBackground(Color.cyan);
                panel2.setBackground(Color.cyan);
                panel3.setBackground(Color.cyan);
		frame.add(panel3);
		frame.validate();	
	}
	private class CustomTableModule extends DefaultTableModel
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 6291865013403466375L;
		private Object[][] data;
		String[] columns = {"Id","Amount", "Type", "Creation Date", "Owner Name"};;
		public CustomTableModule(Object[][] data)
		{
			this.data = data;
		}
		
		public int getRowCount() { return data == null ? 0 : data.length; }
                public int getColumnCount() { return data == null ? 0 : 5;}
		
		public boolean isCellEditable(int row, int col)
		{
			return false;
		}
		
		public Object getValueAt(int row, int col) 
		{
		    return data[row][col];
		}
		
		 public String getColumnName(int col)
		 {
		        return columns[col];
		 }
		public void setValueAt(Object value, int row, int col)
		{
		   data[row][col] = value;
		   fireTableCellUpdated(row, col);
		}
	}
	
	private class TableKeyListener implements KeyListener
	{
		public void keyPressed(KeyEvent arg0) {
		}

		public void keyReleased(KeyEvent arg0) {
		}

		public void keyTyped(KeyEvent arg0) {	
			if(arg0.getKeyChar() == KeyEvent.VK_ENTER)
			{
				int row = table.getSelectedRow();
				row--;
				if(row == -1)
					row = table.getRowCount() - 1;
				Account account = accounts.get(row);
				int id = account.getIdAccount();
				accountSpinner.setValue(id);
			}
		}
	}
	private void loadAccounts()
	{
		ArrayList<Client> clients = Client.readAllClients();
		accounts = new ArrayList<Account>();
		for(Client c : clients)
		{
			if(c.getName().equals("Utilities Company"))
				continue;
			for(Account a : c.getAccount())
				if(a.getIdAccount() != account.getIdAccount())
					accounts.add(a);
		}
		String[][] data = new String[accounts.size()][];
		int i = 0;
		for(Account a : accounts)
		{
			data[i] = new String[5];
			data[i][0] = Integer.toString(a.getIdAccount());
			data[i][1] = Integer.toString(a.getAmount());
			data[i][2] = a.getType();
			data[i][3] = a.getCreationDate();
			data[i][4] = a.getOwner().getName();
			i++;
		}
		
		table = new JTable(new CustomTableModule(data));
		table.addKeyListener(new TableKeyListener());
		panel2.add(table, BorderLayout.CENTER);
	}
}
