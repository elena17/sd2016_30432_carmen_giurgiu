package UserInterface;
import java.awt.BorderLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.*;

import DomainLayer.Employee;
import DomainLayer.Transaction;
import java.awt.Color;

public class ReportWindow {
	private JFrame frame;
	private AdministratorWindow window;
        
	private class FrameWindowListener implements WindowListener
	{
            
                @Override
		public void windowActivated(WindowEvent arg0) {
		}
                @Override
		public void windowClosed(WindowEvent arg0) {
		}
                @Override
		public void windowClosing(WindowEvent arg0) {	
			window.makeVisible();
		}
                @Override
		public void windowDeactivated(WindowEvent arg0) {
		}
                @Override
		public void windowDeiconified(WindowEvent arg0) {
		}
                @Override
		public void windowIconified(WindowEvent arg0) {
		}
                @Override
		public void windowOpened(WindowEvent arg0) {
		}		
	}
        
        
	public ReportWindow(Employee employee, AdministratorWindow window)
	{
		frame = new JFrame();
		frame.setSize(800, 600);
                frame.setLocation(400, 80);
		frame.setVisible(true);
		this.window = window;
		frame.addWindowListener(new FrameWindowListener());
		JPanel panel1 = new JPanel(new BorderLayout());
                panel1.setBackground(Color.cyan);
		ArrayList<Transaction> transactions = Transaction.getTransactionByEmployee(employee.getIdEmployee());
		String s = " Report for employee " + employee.getName() + "\n";
		for(Transaction t : transactions)
		{
			s += " on" + t.getDate() + " the employee performed a ";
			switch(t.getType())
			{
			case DEPOSIT:	s += "deposit ";
							break;
			case WITHDRAW:	s += "withdrawal ";
							break;
			case TRANSFER:	s += "transfer ";
							break;		
			}
			s += "amounting to a value of " + Math.abs(t.getAmount()) + " for the account with the id = " + t.getIdFromAccount();
			if(t.getType() == Transaction.TransactionType.TRANSFER)
				s += " to the account with the id = " + t.getIdToAccount();
			s += "\n";
		}
		JTextArea textArea = new JTextArea();
		textArea.setText(s);
		textArea.setEditable(false);
		panel1.add(textArea, BorderLayout.CENTER);
		frame.add(panel1);	
		frame.validate();	
	}
}
