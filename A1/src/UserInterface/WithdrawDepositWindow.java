package UserInterface;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import DomainLayer.Account;
import DomainLayer.Employee;
import DomainLayer.Transaction;
import java.awt.Color;

public class WithdrawDepositWindow {
	private JFrame frame;
	private AccountWindow window;
	private JSpinner spinner;
	private Account account;
	private WithdrawDepositWindowType type;
	private class FrameWindowListener implements WindowListener
	{
                @Override
		public void windowActivated(WindowEvent arg0) {
		}
                @Override
		public void windowClosed(WindowEvent arg0) {
		}
                @Override
		public void windowClosing(WindowEvent arg0) {	
			window.makeVisible();
		}
                @Override
		public void windowDeactivated(WindowEvent arg0) {
		}
                @Override
                public void windowDeiconified(WindowEvent arg0) {
		}
                @Override
		public void windowIconified(WindowEvent arg0) {
		}
                @Override
		public void windowOpened(WindowEvent arg0) {
		}
		
	}
	
	public enum WithdrawDepositWindowType
	{
		DEPOSIT, WITHDRAW, PAY_BILLS;
	}
	
	private class ButtonActionListener implements ActionListener 
	{

		public void actionPerformed(ActionEvent arg0) {
			int amount = (Integer) spinner.getValue();
			switch(type)
			{
			case DEPOSIT: account.setAmmount(account.getAmount() + amount);	 
				break;
			case WITHDRAW: 
					account.setAmmount(account.getAmount() - amount);
					amount = -amount;
			  	break;
			case PAY_BILLS:
				account.setAmmount(account.getAmount() - amount);
				break;
			}
			 if(type != WithdrawDepositWindowType.PAY_BILLS)
				 Transaction.create(Employee.getLoggedInEmployee().getIdEmployee(), amount, account.getIdAccount());
			 else
				 Transaction.create(Employee.getLoggedInEmployee().getIdEmployee(), amount, account.getIdAccount(), 4);
			 account.updateAccount();
			 window.makeVisible();
			 window.displayAccounts();
			 frame.dispose();
		}
		
	}
	public WithdrawDepositWindow(AccountWindow window, final Account account, WithdrawDepositWindowType type) 
	{
		this.window = window;
		this.account = account;
		this.type = type;
		frame = new JFrame();
		frame.setSize(800, 600);
                frame.setLocation(400, 80);
		frame.setVisible(true);
		frame.addWindowListener(new FrameWindowListener());
		SpinnerNumberModel model = new SpinnerNumberModel();
		model.setMinimum(0);
		if(type != WithdrawDepositWindowType.DEPOSIT)
			model.setMaximum(account.getAmount());
		JPanel panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		panel1.add(new JLabel("Amount:"), BorderLayout.WEST);
		
		spinner = new JSpinner(model);
		if(type != WithdrawDepositWindowType.DEPOSIT)
			((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().addKeyListener(new KeyListener(){

				public void keyPressed(KeyEvent arg0) {
				}
				public void keyReleased(KeyEvent arg0) {
				}
				public void keyTyped(KeyEvent arg0) {
					if(arg0.getKeyChar() == KeyEvent.VK_ENTER || arg0.getKeyChar() == '\n')
					{
						int value = (int) spinner.getValue();
						spinner.repaint();
						/*if(value > account.getAmount())
						{
							spinner.setValue(account.getAmount());
							spinner.repaint();
						}*/
					}
				}
				
			});
		panel1.add(spinner, BorderLayout.CENTER);
		JPanel panel2 = new JPanel();
		String buttonLabel = "";
		switch(type)
		{
		case DEPOSIT: buttonLabel = " Deposit amount";
					break;
		case WITHDRAW: buttonLabel = " Withdaraw amount";
					break;
		case PAY_BILLS:
					buttonLabel = " Pay bills";
					break;
		}
		JButton button = new JButton(buttonLabel);
		button.addActionListener(new ButtonActionListener());
		panel2.add(button);
		JPanel panel3 = new JPanel(new BorderLayout());
		panel3.add(panel1, BorderLayout.NORTH);
		panel3.add(panel2, BorderLayout.CENTER);
                
                 panel1.setBackground(Color.cyan);
                panel2.setBackground(Color.cyan);
                panel3.setBackground(Color.cyan);
                
		frame.add(panel3);
		frame.validate();
               
	}
	
}
