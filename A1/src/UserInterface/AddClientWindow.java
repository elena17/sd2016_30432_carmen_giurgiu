package UserInterface;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import DomainLayer.Client;
import java.awt.Color;

public class AddClientWindow {
	private JTextField nameTextField, identityCardTextField, personalNumericCodeTextField, addressTextField;
	private JFrame frame;
	private MainWindow window;
	private class AddClientButtonListener implements ActionListener
	{

		public void actionPerformed(ActionEvent arg0) 
		{
			String name = nameTextField.getText();
			String cardNumber = identityCardTextField.getText();
			String cnp = personalNumericCodeTextField.getText();
			String address = addressTextField.getText();
			
			if(name.isEmpty())
			{
				JOptionPane.showMessageDialog(frame, "Name cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(!name.matches("[ ([A-Z][a-z]+)]+"))
			{
				JOptionPane.showMessageDialog(frame, "Name must contain only letters and spaces", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(!cardNumber.matches("[A-Z][A-Z][0-9]{6}"))
			{
				JOptionPane.showMessageDialog(frame, "Idendtity card number invalid", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(!cnp.matches("[1-6][0-9]{12}"))
			{
				JOptionPane.showMessageDialog(frame, "Personal numeric code must be composed of 13 digits", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(address.isEmpty())
			{
				JOptionPane.showMessageDialog(frame, "Address cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			if (Client.getClient(name, cnp) != null){
                            	JOptionPane.showMessageDialog(frame, "Client exists!", "Error", JOptionPane.ERROR_MESSAGE);
				return;		

                        }else
			Client.createClient(name, cardNumber, cnp, address);
			window.displayClients();
			frame.dispose();
		}
		
	}
	
	public AddClientWindow(MainWindow window)
	{
		frame = new JFrame();
		frame.setSize(600, 200);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.window = window;
		JPanel panel1 = new JPanel();
		nameTextField = new JTextField();
		nameTextField.setToolTipText("Must contain only letters and spaces");
		identityCardTextField = new JTextField();
		identityCardTextField.setToolTipText("Must contain exactly 8 characters, the first 2 letters and the others digits");
		personalNumericCodeTextField = new JTextField();
		personalNumericCodeTextField.setToolTipText("Must contain exactly 13 digits");
		addressTextField = new JTextField();
		panel1.setLayout(new GridLayout(4, 2));
		panel1.add(new JLabel("Name:"));
		panel1.add(nameTextField);
		panel1.add(new JLabel("Identity Card Number:"));
		panel1.add(identityCardTextField);
		panel1.add(new JLabel("Personal Numeric Code:"));
		panel1.add(personalNumericCodeTextField);
		panel1.add(new JLabel("address:"));
		panel1.add(addressTextField);
		JPanel panel2  = new JPanel();
		JPanel panel3 = new JPanel();
		JButton button = new JButton("Add Client");
		button.addActionListener(new AddClientButtonListener());
		panel2.add(button);
		panel3.setLayout(new BorderLayout());
		panel3.add(panel1, BorderLayout.NORTH);
		panel3.add(panel2, BorderLayout.EAST);
                
                panel1.setBackground(Color.cyan);
                panel2.setBackground(Color.cyan);
                panel3.setBackground(Color.cyan);
                
                
		frame.add(panel3);
		frame.validate();
		frame.setVisible(true);
	}
	
}
