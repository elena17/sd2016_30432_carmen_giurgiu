package UserInterface;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import DomainLayer.Employee;
import java.awt.Color;

public class AddEmployeeWindow
{
	private JTextField nameTextField, usernameTextField, passwordTextField;
	private JFrame frame;
	private JCheckBox administratorCheckBox;
	private AdministratorWindow window;
	
	private class AddEmployeeButtonListener implements ActionListener
	{

		public void actionPerformed(ActionEvent arg0) 
		{
			String name = nameTextField.getText();
			if(name.isEmpty())
			{
				JOptionPane.showMessageDialog(frame, "Name cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(!name.matches("[ ([A-Z][a-z]+)]+"))
			{
				JOptionPane.showMessageDialog(frame, "Name must contain only letters and spaces", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			String username = usernameTextField.getText();
			if(username.isEmpty())
			{
				JOptionPane.showMessageDialog(frame, "Username cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(username.length() < 5)
			{
				JOptionPane.showMessageDialog(frame, "Username must contain at least 5 characters", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			String password = passwordTextField.getText();
			if(password.isEmpty())
			{
				JOptionPane.showMessageDialog(frame, "Password cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(password.length() < 5)
			{
				JOptionPane.showMessageDialog(frame, "Password must contain at least 5 characters", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			boolean isAdministrator = administratorCheckBox.isSelected();
                        if (Employee.getEmployee(username, password)!= null ){
                            JOptionPane.showMessageDialog(frame, "Employee exists!", "Error", JOptionPane.ERROR_MESSAGE);
				return;
                        }else
			Employee.createEmployee(name, username, password, isAdministrator);
			window.displayEmployees();
			frame.dispose();
		}
		
	}
	public AddEmployeeWindow(AdministratorWindow window)
	{
		frame = new JFrame();
		frame.setSize(600, 200);
                frame.setLocation(400, 80);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.window = window;
		JPanel panel1 = new JPanel();
		nameTextField = new JTextField();
		usernameTextField = new JTextField();
		usernameTextField.setToolTipText("Must contain at least 6 characters");
		passwordTextField = new JPasswordField();
		passwordTextField.setToolTipText("Must contain at least 6 characters");
		panel1.setLayout(new GridLayout(4, 2));
		panel1.add(new JLabel("Name:"));
		panel1.add(nameTextField);
		panel1.add(new JLabel("Username:"));
		panel1.add(usernameTextField);
		panel1.add(new JLabel("Password:"));
		panel1.add(passwordTextField);
		administratorCheckBox = new JCheckBox();
		administratorCheckBox.setSelected(false);
		panel1.add(new JLabel("administrator rights"));
		panel1.add(administratorCheckBox);
		JPanel panel2  = new JPanel();
		JPanel panel3 = new JPanel();
		JButton button = new JButton("Add Employee");
		button.addActionListener(new AddEmployeeButtonListener());
		panel2.add(button);
		panel3.setLayout(new BorderLayout());
		panel3.add(panel1, BorderLayout.NORTH);
		panel3.add(panel2, BorderLayout.EAST);
                
                panel1.setBackground(Color.cyan);
                panel2.setBackground(Color.cyan);
                panel3.setBackground(Color.cyan);
                
		frame.add(panel3);
		frame.validate();
		frame.setVisible(true);
	}
	
}
