package UserInterface;


import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.Color;

import javax.swing.*;

import DomainLayer.Employee;

public class LoginWindow {
	private JTextField usernameTextField, passwordTextField;
	private JFrame frame;
	private JButton loginButton;
	private class LoginButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) 
		{
			String username = usernameTextField.getText().trim();
			String password = passwordTextField.getText().trim();
                        if (usernameTextField != null && passwordTextField != null){
                                Employee employee = Employee.getEmployee(username, password);
                                
                                if(employee != null){
                                        Employee.setLoggedInEmployee(employee);
                                        if(employee.isAdmin())
                                                new AdministratorWindow();
                                                 
                                        else
                                                new MainWindow();
                                        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                                        frame.dispose();
                                } else{
                                    JOptionPane.showMessageDialog(loginButton, "Username or password incorrect"); 
                                }
                        }else{
                            JOptionPane.showMessageDialog(loginButton, "Fill both fields!");
                        }
			
		}
		
	}
	
	private class TextKeyListener implements KeyListener
	{
                @Override
		public void keyPressed(KeyEvent arg0) {
		}

                @Override
		public void keyReleased(KeyEvent arg0) {
		}

                @Override
		public void keyTyped(KeyEvent arg0) {
			if(arg0.getKeyChar() == KeyEvent.VK_ENTER)
				loginButton.doClick();
		}
		
	}
	public LoginWindow()
	{
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 500);
                frame.setLocation(400, 80);
                
		
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
                JPanel panel4 = new JPanel();
                JPanel panel5 = new JPanel();
               
		panel1.setLayout(new BorderLayout());
                
                panel1.setBackground(Color.cyan);                
                panel3.setBackground(Color.cyan);
                panel4.setBackground(Color.cyan);
                panel5.setBackground(Color.cyan);
                
		
		panel2.setLayout(new GridLayout(2, 1));
		usernameTextField = new JTextField();
		passwordTextField = new JPasswordField();
		
		panel4.setLayout(new BorderLayout());
		panel4.add(new JLabel("Username:"), BorderLayout.WEST);
		panel4.add(usernameTextField, BorderLayout.CENTER);
                
		panel5.setLayout(new BorderLayout());
		panel5.add(new JLabel("Password:"), BorderLayout.WEST);
		panel5.add(passwordTextField, BorderLayout.CENTER);
                
                                
                
		panel2.add(panel4);
		panel2.add(panel5);
                
               
		loginButton = new JButton("login");
		loginButton.addActionListener(new LoginButtonListener());
		panel3.add(loginButton);
		panel1.add(panel2, BorderLayout.NORTH);
		panel1.add(panel3, BorderLayout.EAST);
		usernameTextField.addKeyListener(new TextKeyListener());
		passwordTextField.addKeyListener(new TextKeyListener());
		frame.add(panel1);
		frame.validate();
		frame.setVisible(true);
	}
	
	public static void main(String[] args)
	{
		new LoginWindow();
	}
}
