package DataAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AccountGateway {	        
      
        /**
         * 
         * @param idclient
         * @param amount
         * @param type 
         */
        public static void create(int idclient, int amount, String type){
            //String sql = "insert into account (idclient, idtype, amount, creationdate) values ("+idclient+", (select id from accounttype where type = '"+type+"'),"+amount+", select now()"+")";   
            String sql = " insert into account (idclient, amount, idtype, creationDate)";
		sql += " values(" + idclient +"," + amount + ", (select idtype from accounttype where type = '" + type +"'),";
		sql += "(select now()))"; 
            try {
                Database.getInstance().executeUpdate(sql);
            }catch(SQLException e){
                e.printStackTrace();;
            }
        }
        
        
	
	public static void update(int idaccount, int amount, String type)
	{
		String sql = " update account set amount = " + amount + ",";
		sql += " idtype = (select idtype from accounttype where type ='" + type + "') ";
		sql += "where idaccount =" + idaccount;
		try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        
        
        
	
	public static void delete(int id)
	{
		String sql = " delete from account where idaccount =" + id;
		try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static ResultSet findAccountsByClientId(int idclient)
	{
		String sql = "select idaccount, amount, type, ";
		sql +="creationDate from account, accounttype where idclient = " + idclient;
		sql += " and account.idtype = accounttype.idtype";
		try {
			return Database.getInstance().executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static ResultSet findPossibleAccountTypes()
	{
		String sql = "select type from accounttype";
		try {
			return Database.getInstance().executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
        
        public static ResultSet findAllAccounts()
	{
		String sql = " select idaccount, amount, type, creationdate from account, accounttype where account.idtype = accounttype.idtype";
		try {
			return Database.getInstance().executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
        
     public static void main (String [] args){
         AccountGateway a = new AccountGateway();
         
                  
            ArrayList<String> arr = new ArrayList(); 
            String st = null;
            
            ResultSet rs = AccountGateway.findAccountsByClientId(1);
            
            try {                 
                   while(rs.next()){                      
                        arr.add(rs.getString("idaccount"));
                        arr.add(rs.getString("amount"));
                        arr.add(rs.getString("type"));
                        arr.add(rs.getString("creationdate"));
                        
                    }
                   
                    st = String.join(" ", arr);
                    System.out.println(st);
                    
                }
            catch (Exception ae) {
                ae.printStackTrace();
            }
     }
        
        
        
}
