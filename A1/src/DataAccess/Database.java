package DataAccess;
import java.sql.*;
import java.io.*;

public class Database {
    private static Database database = null;
    private static Connection conn = null;
    
    private String host = "jdbc:mysql://localhost:3306/bank?zeroDateTimeBehavior=convertToNull";
    private String uName = "root";
    private String uPass = "";
    
    private java.sql.Statement stm = null;
    
    public Database(){ 
        
        try{            
            conn = DriverManager.getConnection(host, uName, uPass);            
        }catch(SQLException err){
            err.printStackTrace();
        }
        
    }
    
    
    public static Database getInstance(){
        if (database == null)
            database = new Database();
        return database;
    }
    
    public void executeUpdate(String updateString) throws SQLException
    {
        stm = conn.createStatement();
        stm.executeUpdate(updateString);
    }
    
    public ResultSet executeQuery(String selectString) throws SQLException
    {
        stm = conn.createStatement();
        return stm.executeQuery(selectString);
    }
    
    @Override
    protected void finalize() throws Throwable
    {
        conn.close();
    }   
    
}
