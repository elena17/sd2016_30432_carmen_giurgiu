package DataAccess;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeGateway 
{
	public static ResultSet findEmployee(String username, String password)
	{
		String sql = "select * from employee where username ='" + username + "' and password='" + password + "'";
		try {
			return Database.getInstance().executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void delete(int id)
	{
		String sql = "delete from employee where idemployee=" + id;
		try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void create(String name, String username, String password, boolean isadmin)
	{
		String sql = "insert into employee(name, username, password, isadmin) values('";
		sql += name +"','" + username + "','" + password + "'," + (isadmin ? 1 : 0) + ")";
		try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void update(int idemployee, String username, String name, String password, boolean isadmin)
	{
		String sql = " update employee set name='" + name + "', username='";
		sql += username + "', password = '" + password + "', isadmin = "; 
		sql += (isadmin ? 1 : 0) + " where id=" + idemployee;
		try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static ResultSet findAllEmployees()
	{
		String sql = " select * from employee";
		try {
			return Database.getInstance().executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
