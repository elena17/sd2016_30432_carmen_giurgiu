package DataAccess;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TransactionGateway {
	public static void create(int idemployee, int amount, int idfromaccount)
	{
		String sql = "insert into transaction(idemployee, amount, idfromaccount, date) ";
		sql += "values(" + idemployee + "," + amount + "," + idfromaccount + ", (select now()))";
		try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void create(int idemployee, int amount, int idfromaccount, int idtoaccount)
	{
		String sql = "insert into transaction(idemployee, amount, idfromaccount, idtoaccount, date) ";
		sql += "values(" + idemployee + "," + amount + "," + idfromaccount + "," + idtoaccount + ", (select now()))";
		try {
			Database.getInstance().executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static ResultSet findTransactionsByEmployeeId(int idemployee)
	{
		String sql = "select * from transaction where idemployee = " + idemployee;
		try {
			return Database.getInstance().executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
