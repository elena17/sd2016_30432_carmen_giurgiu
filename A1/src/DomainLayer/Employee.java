package DomainLayer;

import DataAccess.EmployeeGateway;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 *
 * @author Giurgiu Carmen
 */


public class Employee {
	
	private String name, username, password;
	private int idEmployee;
	private static Employee loggedInEmployee;
	private boolean isAdmin;
	
	public Employee(int idEmployee, String name, String username, String password, boolean isAdmin){
            this.idEmployee = idEmployee;
            this.name = name;
            this.username = username;
            this.password = password;
            this.isAdmin = isAdmin;
        }
        
        public Employee( String name, String username, String password, boolean isAdmin){            
            this.name = name;
            this.username = username;
            this.password = password;
            this.isAdmin = isAdmin;
        }
        
        
        
        public int getIdEmployee(){
		return idEmployee;
	}

	public void setId(int idEmployee){
		this.idEmployee = idEmployee;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getUsername(){
		return username;
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public static Employee getLoggedInEmployee(){
		return loggedInEmployee;
	}
	
	public static void setLoggedInEmployee(Employee employee){
		loggedInEmployee = employee;
	}

	public boolean isAdmin() 
	{
		return isAdmin;
	}

	public void setAdministrator(boolean isAdmin)
	{
		this.isAdmin = isAdmin;
	}

	
	
	public void delete(){
		EmployeeGateway.delete(idEmployee);
	}
	
	public void update(){
		EmployeeGateway.update(idEmployee, username, name, password, isAdmin);
	}
        
        
	
	public static Employee getEmployee(String username, String password)
	{
		ResultSet result = EmployeeGateway.findEmployee(username, password);
		if(result != null)
		{
			try
			{
				if(!result.next())
					return null;
				int id = result.getInt("idemployee");
				String name = result.getString("name");
				boolean isAdmin = result.getBoolean("isadmin");
				return new Employee(id, name, username, password, isAdmin);
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static ArrayList<Employee> getAllEmployees()
	{
		ArrayList<Employee> employees = new ArrayList<Employee>();
		ResultSet result = EmployeeGateway.findAllEmployees();
		if(result != null)
		{
			try {
				while(result.next())
				{
					int id = result.getInt("idemployee");
					String name = result.getString("name");
					String username = result.getString("username");
					String password = result.getString("password");
					boolean isAdmin = result.getBoolean("isadmin");
					employees.add(new Employee(id, name, username, password, isAdmin));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return employees;
	}
        
        
	public static void createEmployee(String name, String username, String password, boolean isAdmin){
		EmployeeGateway.create(name, username, password, isAdmin);
	}
        
}
