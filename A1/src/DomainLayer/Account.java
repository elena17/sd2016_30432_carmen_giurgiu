package DomainLayer;

import DataAccess.AccountGateway;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Giurgiu Carmen
 */
public class Account {
    private Client owner = null;
    private int idAccount;
    private int amount;
    private String type;
    private String creationDate;
    
    public Account(int idAccount, int amount, String type, String creationDate){
        this.idAccount = idAccount;
        this.amount = amount;
        this.type = type;
        this.creationDate = creationDate;
        
    }
    
    public void setIdAccount(int idAccount){
        this.idAccount = idAccount;
    }
    
    public int getIdAccount(){
        return idAccount;
    }
    
    public void setOwner (Client owner){
        this.owner = owner;
    }
    
    public Client getOwner(){
        return owner;
    }
    
    public void setAmmount(int amount){
        this.amount = amount;
    }
    
    public int getAmount(){
        return amount;
    }
    
    public void setType(String type){
        this.type = type;        
    }
    
    public String getType(){
        return type;
    }
    
    public void setCreationDate(String creationDate){
        this.creationDate = creationDate;
    }
    
    public String getCreationDate(){
        return creationDate;
    }
    
    
    public static void createAccount(int idClient, int amount, String type){
        AccountGateway.create(idClient, amount, type);
    }
    
    public void deleteAccount(){
        AccountGateway.delete(idAccount);
    }
    
    
    public void updateAccount(){
        AccountGateway.update(idAccount, amount, type);       
    }
    
    public static ArrayList<Account> findAccountByClientId(int idClient){
        ResultSet result = AccountGateway.findAccountsByClientId(idClient);
        
        ArrayList<Account> account = new ArrayList<Account>();
        if (result != null){
            try{
                while(result.next()){
                    int id = result.getInt("idaccount");
                    int amount = result.getInt("amount");
                    String type =result.getString("type");
                    String creationDate = result.getString("creationdate");
                    account.add(new Account(id, amount, type, creationDate));
                }
                
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
        
        
        return account;
    }
    
    public static ArrayList<Account> readAllAccounts(){
		ArrayList<Account> accounts = new ArrayList<Account>();
		ResultSet result = AccountGateway.findAllAccounts();
                
		if(result != null)
		{
			try {
				while(result.next())
				{
					int id = result.getInt("idaccount");
                                        int amount = result.getInt("amount");
                                        String type =result.getString("type");
                                        String creationDate = result.getString("creationdate");
                                        Account account = new Account(id, amount, type, creationDate);
                                        accounts.add(account);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return accounts;
	}
    
    public static ArrayList<String> getPossibleAccountTypes() {
		ArrayList<String> types = new ArrayList<String>();
		ResultSet result = AccountGateway.findPossibleAccountTypes();
		if(result != null)
		{
			try {
				while(result.next())
				{
					String type = result.getString("type");
					types.add(type);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return types;
	}

}













