package DomainLayer;


import DataAccess.AccountGateway;
import DataAccess.ClientGateway;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 *
 * @author Giurgiu Carmen
 */


public class Client {
	private int idClient;
	private String name;
	private String cardNumber;
	private String cnp;
	private String address;
	private ArrayList<Account> account;
	public Client(int id, String name, String cardNumber, String cnp, String address)
	{
		this.idClient = id;
		this.name = name;
		this.cardNumber = cardNumber;
		this.cnp = cnp;
		this.setAddress(address);
		readAccounts();
	}
	
	public ArrayList<Account> readAccounts()
	{
		account = Account.findAccountByClientId(idClient);
		for(Account a : account)
			a.setOwner(this);
                
                return account;
	}
        
        
	public int getIdClient(){
		return idClient;
	}
        
	public void setId(int idClient){
		this.idClient = idClient;
	}
        
	public String getName(){
		return name;
	}
        
	public void setName(String name){
		this.name = name;
	}
        
	public String getCardNumber(){
		return cardNumber;
	}
        
	public void setIdentityCardNumber(String cardNumber){
		this.cardNumber = cardNumber;
	}
        
	public String getCnp(){
		return cnp;
	}
        
	public void setCnp(String cnp){
		this.cnp = cnp;
	}
        
	public String getAddress(){
		return address;
	}
        
        
	public void setAddress(String address){
		this.address = address;
	}
        
        
	public ArrayList<Account> getAccount()	{
		return account;
	}
        
	public void setAccount(ArrayList<Account> account){
		this.account = account;
	}
        
        
	
	public void update(){
		ClientGateway.update(idClient, name, cardNumber, cnp, address);
	}
        
        
	public static ArrayList<Client> readAllClients(){
		ArrayList<Client> clients = new ArrayList<Client>();
		ResultSet result = ClientGateway.findAllClients();
                
		if(result != null)
		{
			try {
				while(result.next())
				{
					int id = result.getInt("idclient");                                        
					String name = result.getString("name");
					String cardNumber = result.getString("cardnumber");
					String cnp = result.getString("cnp");
					String address = result.getString("address");
					Client client = new Client(id, name, cardNumber, cnp, address);
					clients.add(client);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return clients;
	}
        
        
        public static Client getClient(String name, String cnp)
	{
		ResultSet result = ClientGateway.findClient(name, cnp);
		if(result != null)
		{
			try
			{
				if(!result.next())
					return null;
				int id = result.getInt("idclient");
				String cardnumber = result.getString("cardnumber");
                                name = result.getString("name");
                                cnp = result.getString("cnp");
				String address = result.getString("address");
				return new Client(id, name, cardnumber, cnp, address);
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}
	
        
        
	public static void createClient(String name, String cardNumber, String cnp, String address){
		ClientGateway.create(name, cardNumber, cnp, address);
	}
        
        public void delete(){
		ClientGateway.delete(idClient);
	}
        
        
}