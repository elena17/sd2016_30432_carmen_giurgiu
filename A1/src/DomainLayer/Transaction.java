package DomainLayer;

import DataAccess.TransactionGateway;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Giurgiu Carmen
 */
public class Transaction {
	public enum TransactionType{
		DEPOSIT, WITHDRAW, TRANSFER
	}
	private TransactionType type;
        private int idTransaction;
	private int idEmployee;
	private int amount;
	private int idFromAccount;	
	private int idToAccount = -1;
	private String date;
        
	public Transaction (int idTransaction, TransactionType type, int idEmployee, int amount, int idFromAccount, String date){
		this.idTransaction = idTransaction;
                this.type = type;
                this.idEmployee = idEmployee;
                this.amount = amount;
                this.idFromAccount = idFromAccount;
                this.date = date;
	}
        
	public Transaction (int idTransaction, TransactionType type, int idEmployee, int amount, int idFromAccount, int idToAccount, String date){
		this.idTransaction = idTransaction;
                this.type = type;
                this.idEmployee = idEmployee;
                this.amount = amount;
                this.idFromAccount = idFromAccount;
                this.idToAccount = idToAccount;
                this.date = date;
	}
        
        public int getIdTransation(){
		return idTransaction;
	}
        
	public void setIdTransaction(int idTransaction){
		this.idTransaction = idTransaction;
	}
        
	public TransactionType getType(){
		return type;
	}
	public void setType(TransactionType type){            
		this.type = type;
	}
        
	public int getEmployeeId(){
		return idEmployee;
	}
        
	public void setEmployeeId(int employeeId) {
		this.idEmployee = idEmployee;
	}
        
	public int getAmount(){
		return amount;
	}
        
	public void setAmount(int amount){
		this.amount = amount;
	}
        
	public int getIdFromAccount(){
		return idFromAccount;
	}
        
	public void setIdFromAccount(int idFromAccount) {
		this.idFromAccount = idFromAccount;
	}
        
	public int getIdToAccount() {
		return idToAccount;
	}
        
        public void setIdToAccount (int idToAccount){
            this.idToAccount = idToAccount;
        }
        
	public String getDate(){
		return date;
	}
        
	public void setDate(String date){
		this.date = date;
	}
        
	
	
	public static void create(int idEmployee, int amount, int idFomAccount) {
		TransactionGateway.create(idEmployee, amount, idFomAccount);
	}
        
	public static void create(int idEmployee, int amount, int idFomAccount, int idToAccount) {
		TransactionGateway.create(idEmployee, amount, idFomAccount, idToAccount);
	}
        
        
	public static ArrayList<Transaction> getTransactionByEmployee(int idEmployee) {
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();
		ResultSet result = TransactionGateway.findTransactionsByEmployeeId(idEmployee);
		if(result != null)
		{
			try {
				while(result.next())
				{
					int id = result.getInt("idtransaction");
					int amount = result.getInt("amount");
					int idFromAccount = result.getInt("idfromaccount");
					int idToAccount;// = result.getInt("targetAccountId");
					String test = result.getString("idtoaccount");
					if(result.wasNull())
						idToAccount = -1;
					else
						idToAccount = Integer.parseInt(test);
					String date = result.getString("date");
					if(idToAccount == -1)
					{
						TransactionType type = amount >= 0 ? TransactionType.DEPOSIT : TransactionType.WITHDRAW;
						transactions.add(new Transaction(id, type, idEmployee, amount, idFromAccount, date));
					}
					else
						transactions.add(new Transaction(id, TransactionType.TRANSFER, idEmployee, amount, idFromAccount, idToAccount, date));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return transactions;
	}
}