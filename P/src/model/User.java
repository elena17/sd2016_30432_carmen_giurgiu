package model;

/**
 *
 * @author Giurgiu Carmen
 */
public class User {
    private int id;
    private String name;
    private String username;
    private String password;
    private String type;
    
    
    public User(){
        
    }
    
    public User(int id, String name, String username, String password, String type){
        this.id  = id;
        this.name = name;
        this.username = username; 
        this.password = password;
        this.type = type;
    }
    
    
    public void setId(int id) {

        this.id = id;
    }

    public int getId() {

        return id;
    }
    
    
    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }
    
    
    public void setUsernameame(String username) {

        this.username = username;
    }

    public String getUsername() {

        return username;
    }
    
    
    public void setPassword(String password) {

        this.password = password;
    }

    public String getPassword() {

        return password;
    }
    
    
    public void setType(String type) {

        this.type = type;
    }

    public String getType() {

        return type;
    }
    
}
