package model;

/**
 *
 * @author Giurgiu Carmen
 */
public class HotelBooking {
    private int id;
    private int ticketId;
    private int hotelId;
    private String country;      
    private int noOfRooms;
    private int noOfNights;
    private double totalPrice;
    
    
    public HotelBooking(){
        
    }
    
    public HotelBooking(int id, int ticketId, int hotelId, String country, int noOfRooms, int noOfNights, double totalPrice){
        this.id = id;
        this.ticketId = ticketId;
        this.hotelId = hotelId;
        this.country = country;
        this.noOfRooms = noOfRooms;
        this.noOfNights = noOfNights;
        this.totalPrice = totalPrice;
    }
    
    
    
    
     public void setId(int id) {

        this.id = id;
    }

    public int getId() {

        return id;
    }
    
    
    
    
    
    
     public void setTicketId(int ticketId) {

        this.ticketId = ticketId;
    }

    public int getTicketId() {

        return ticketId;
    }
    
    
    
    public void setHotelId(int hotelId) {

        this.hotelId = hotelId;
    }

    public int getHotelId() {

        return hotelId;
    }
    

    public void setCountry(String country) {

        this.country = country;
    }

    public String getCountry() {

        return country;
    }    
   
    

    public void setNoOfRooms(int noOfRooms) {

        this.noOfRooms = noOfRooms;
    }

    public int getNoOfRooms() {

        return noOfRooms;
    }
   

    public void setNoOfNights(int noOfNights) {

        this.noOfNights = noOfNights;
    }

    public int getNoOfNights() {

        return noOfNights;
    }
    
    
    
    public void setTotalPrice(double totalPrice) {

        this.totalPrice = totalPrice;
    }

    public double getTotalPrice() {

        return totalPrice;
    }
}
