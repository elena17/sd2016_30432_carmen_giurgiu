package model;

/**
 *
 * @author Giurgiu Carmen
 */
public class Country {
    private int id;
    private String countryName;
    private int priceOnGoPass;
    private int priceOnGoChildren;
    private int priceOnGoBaby;
    private int priceOnGoBackPass;
    private int priceOnGoBackChildren;
    private int priceOnGoBackBaby;
    
    public Country(){
        
    }
    
    
    public Country(int id, String countryName, int priceOnGoPass, int priceOnGoChildren, int priceOnGoBaby, int priceOnGoBackPass, int priceOnGoBackChildren, int priceOnGoBackBaby){
        this.id = id;
        this.countryName = countryName;
        this.priceOnGoPass = priceOnGoPass;
        this.priceOnGoChildren = priceOnGoChildren;
        this.priceOnGoBaby = priceOnGoBaby;
        this.priceOnGoBackPass = priceOnGoBackPass;
        this.priceOnGoBackChildren = priceOnGoBackChildren;
        this.priceOnGoBackBaby = priceOnGoBackBaby;
    }
    
    
    
     public void setId(int id) {

        this.id = id;
    }

    public int getId() {

        return id;
    }
    
    
    
    
    
    public void setCountryName(String countryName) {

        this.countryName = countryName;
    }

    public String getCountryName() {

        return countryName;
    }
    
    
    

    public void setPriceOnGoPass(int priceOnGoPass) {

        this.priceOnGoPass = priceOnGoPass;
    }

    public int getPriceOnGoPass() {

        return priceOnGoPass;
    }
    
    
    

    public void setPriceOnGoChildren(int priceOnGoChildren) {

        this.priceOnGoChildren= priceOnGoChildren;
    }

    public int getPriceOnGoChildren() {

        return priceOnGoChildren;
    }
   
    
    

    public void setPriceOnGoBaby(int priceOnGoBaby) {

        this.priceOnGoBaby = priceOnGoBaby;
    }

    public int getPriceOnGoBaby() {

        return priceOnGoBaby;
    }
    
    

    public void setPriceOnGoBackPass(int priceOnGoBackPass) {

        this.priceOnGoBackPass = priceOnGoBackPass;
    }

    public int getPriceOnGoBackPass() {

        return priceOnGoBackPass;
    } 
    
    

    public void setPriceOnGoBackChildren(int priceOnGoBackChildren) {

        this.priceOnGoBackChildren = priceOnGoBackChildren;
    }

    public int getPriceOnGoBackChildren() {

        return priceOnGoBackChildren;
    } 
    
    
    

    public void setPriceOnGoBackBaby(int priceOnGoBackBabyy) {

        this.priceOnGoBackBaby = priceOnGoBackBaby;
    }

    public int getPriceOnGoBackBaby() {

        return priceOnGoBackBaby;
    }
}
