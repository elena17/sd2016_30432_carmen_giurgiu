package model;

/**
 *
 * @author Giurgiu Carmen
 */
public class Airport {
     private String airportName;
     private int id;
     
     public Airport(){
         
     }
     
     public Airport(int id, String airportName){
         this.id = id;
         this.airportName = airportName;
     }
     
     
     
     public void setId(int id) {

        this.id = id;
    }

    public int getId() {

        return id;
    }
     
     
     
    public void setAirportName(String airportName) {

        this.airportName = airportName;
    }

    public String getAirportName() {

        return airportName;
    }
}
