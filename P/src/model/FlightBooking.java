package model;

/**
 *
 * @author Giurgiu Carmen
 */
public class FlightBooking {
    private int id;
    private int noOfTicket;
    private String name;
    private String email;
    private String state;
    private String from;
    private String airport;
    private String to;
    private String date;
    private int numOfPassenger;
    private int numOfChildren;
    private int numOfBaby;
    private double totalPrice;
    private String notes;
    
    
    public FlightBooking(){
        
    }
    
    public FlightBooking(int id, int noOfTicket, String name, String email, String state, String from, String airport, String to, String date, int numOfPassenger, int numOfChildren, int numOfBaby, double totalPrice, String notes ){
        this.id = id;
        this.noOfTicket = noOfTicket;
        this.name = name;
        this.email = email;
        this.state = state;
        this.from = from;
        this.airport = airport;
        this.to = to;
        this.date = date;
        this.numOfPassenger = numOfPassenger;
        this.numOfChildren = numOfChildren;
        this.numOfBaby = numOfBaby;
        this.totalPrice = totalPrice;
        this.notes = notes;
    }
    
    public void setId(int id) {

        this.id = id;
    }

    public int getId() {

        return id;
    } 
    
    
    public void setNoOfTicket(int noOfTicket) {

        this.noOfTicket = noOfTicket;
    }

    public int getNoOfTicket() {

        return noOfTicket;
    }  
    
    
    
    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }    

    
    
    public void setEmail(String email) {

        this.email = email;
    }

    public String getEmail() {

        return email;
    }   
    
    

    public void setState(String state) {

        this.state = state;
    }

    public String getState() {

        return state;
    }    

    
    
    
    public void setFrom(String from) {

        this.from = from;
    }

    public String getFrom() {

        return from;
    }    

    
    
    
    public void setAirport(String airport) {

        this.airport = airport;
    }

    public String getAirport() {

        return airport;
    }    

    
    
    
    public void setTo(String to) {

        this.to = to;
    }

    public String getTo() {

        return to;
    }  
    
    

    public void setDate(String date) {

        this.date = date;
    }

    public String getDate() {

        return date;
    }    

    
    
    public void setNumOfPassenger(int numOfPassenger) {

        this.numOfPassenger = numOfPassenger;
    }

    public int getNumOfPassenger() {

        return numOfPassenger;
    }    
    
    
    

    public void setNumOfChildern(int numOfChildren) {

        this.numOfChildren = numOfChildren;
    }

    public int getNumOfChildern() {

        return numOfChildren;
    }   
    
    

    public void setNumOfBaby(int numOfBaby) {

        this.numOfBaby = numOfBaby;
    }

    public int getNumOfBaby() {

        return numOfBaby;
    }    

    
    public void setTotalPrice(double totalPprice) {

        this.totalPrice = totalPrice;
    }

    public double getTotalPrice() {

        return totalPrice;
    }  
    
    

    public void setNotes(String notes) {

        this.notes = notes;
    }

    public String getNotes() {

        return notes;
    }
    
}
