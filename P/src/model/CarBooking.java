package model;

/**
 *
 * @author Giurgiu Carmen
 */
public class CarBooking {
    private int id;
    private int ticketId;
    private String carType;
    private int noOfNights;
    private double totalPrice;
    
    public CarBooking(){
        
    }
    
    
    public CarBooking(int id, int ticketId, String carType, int noOfNights, double totalPrice){
        this.id = id;
        this.ticketId = ticketId;
        this.carType = carType;
        this.noOfNights = noOfNights;
        this.totalPrice = totalPrice;
    }
    
    
     public void setId(int id){
        this.id = id;
    }
    
    public  int getId(){
        return id;
    }
    
    
    
    public void setTicketId(int ticketId){
        this.ticketId = ticketId;
    }
    
    public  int getTicketId(){
        return ticketId;
    }
    
    
    
    public void setCarType(String carType){
        this.carType = carType;
    }
    
    public String getCarType(){
        return carType;
    }
    
    
    public void setNoOfNights(int noOfNights){
        this.noOfNights = noOfNights;
    }
    
    public int getNoOfNights(){
        return noOfNights;
    }
    
    
    public void setTotalPrice(double totalPrice){
        this.totalPrice = totalPrice;
    }
    
    public double getTotalPrice(){
        return totalPrice;
    }
    
}
