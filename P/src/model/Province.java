package model;

/**
 *
 * @author Giurgiu Carmen
 */
public class Province {
    private int id;
    private int airportId;
    private String name;
    
    
    public Province(){
        
    }
    
    
    public Province(int id, int airportId, String name){
        this.id = id;
        this.airportId = airportId;
        this.name = name;
        
    }
    
    
    public void setId(int id){
        this.id = id;
    }
    
    public int getId(){
        return id;
    }
    
    
    
    public void setAirportId(int aiportId){
        this.airportId = aiportId;
    }
    
    public int getAiportId(){
        return airportId;
    }
    
    
    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }
}
