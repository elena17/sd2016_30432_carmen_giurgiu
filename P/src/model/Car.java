package model;

/**
 *
 * @author Giurgiu Carmen
 */
public class Car {
    private int id;
    private String carType;
    private double pricePerDay;
    
    public Car(){
        
    }
    
    public Car(int id, String carType, double pricePerDay){
        this.id = id;
        this.carType = carType;
        this.pricePerDay = pricePerDay;
    }
    
    
    public void setId(int id){
        this.id = id;
    }
    
    public  int getId(){
        return id;
    }
    
    
    
    public void setCarType(String carType){
        this.carType = carType;
    }
    
    public String getCarType(){
        return carType;
    }
    
    
    
    public void setPricePerDay(double pricePerDay){
        this.pricePerDay = pricePerDay;
    }
    
    public double getPricePerday(){
        return pricePerDay;
    }
}
