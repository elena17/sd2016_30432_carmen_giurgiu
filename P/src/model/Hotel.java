package model;

/**
 *
 * @author Giurgiu Carmen
 */
public class Hotel {
    
    private int id;
    private int countryId;
    private String hotelName;
    private double priceAtNight;
    private String phone;
    private String address;
    
    
    
    public Hotel(){
        
    }
    
    
    public Hotel(int id, int countryId, String hotelName, double priceAtNight, String phone, String address){
        this.id = id;
        this.countryId = countryId;
        this.hotelName = hotelName;
        this.priceAtNight = priceAtNight;
        this.phone = phone;
        this.address = address;
    }
    
    
    public void setId(int id) {

        this.id = id;
    }

    public int getId() {

        return id;
    }
    
    
    public void setCountryId(int countryId) {

        this.countryId = countryId;
    }

    public int getCountryId() {

        return countryId;
    }
    
    
    
    public void setHotelName(String hotelName) {

        this.hotelName = hotelName;
    }

    public String getHotelName() {

        return hotelName;
    } 
    
    
    
    public void setPriceAtNight(double priceAtNight) {

        this.priceAtNight = priceAtNight;
    }

    public double getPriceAtNight() {

        return priceAtNight;
    }
    
    
    
    public void setPhone(String phone) {

        this.phone = phone;
    }

    public String getPhone() {

        return phone;
    } 

    
    
    public void setAddress(String address) {

        this.address = address;
    }

    public String getAddress() {

        return address;
    } 
    
}
