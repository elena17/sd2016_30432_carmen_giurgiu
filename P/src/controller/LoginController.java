package controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.User;

/**
 *
 * @author Giurgiu Carmen
 */
public class LoginController {
       private static DBController db = new DBController();
       public static User getUser(String username, String password) {
        User user=new User();
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call login(?,?)}");
            callableStatement.setString(1, username);
            callableStatement.setString(2, password);
            ResultSet resultSet=callableStatement.executeQuery();
            if (resultSet.next()){                
                user=new User(resultSet.getInt(1),resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5) );
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }
       
       
       public static void main(String [] args){
           User user = LoginController.getUser("elena", "pass");
           
           System.out.println("username: " + user.getUsername() + "      password: " + user.getPassword());
       }
}
