package controller;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Giurgiu Carmen
 */
public class DBController {
    private static Connection conn = null;
    
    private String host = "jdbc:mysql://localhost:3306/travel";
    private String uName = "root";
    private String uPass = "";
    
    public Connection getConnection(){
             
        
        try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(host, uName, uPass);
		} catch (Exception e) {
			System.out.println("Error: " + e);
		}
        return conn;
        
    }
}
