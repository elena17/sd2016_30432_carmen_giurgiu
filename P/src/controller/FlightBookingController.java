package controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.FlightBooking;

/**
 *
 * @author Giurgiu Carmen
 */
public class FlightBookingController {
    
    private static DBController db = new DBController();
    
    public static String addFlightBooking(FlightBooking flight){
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call addBooking(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            callableStatement.setInt(1, flight.getId());
            callableStatement.setInt(2, flight.getNoOfTicket());
            callableStatement.setString(3, flight.getName());
            callableStatement.setString(4, flight.getEmail());
            callableStatement.setString(5, flight.getState());
            callableStatement.setString(6, flight.getFrom());
            callableStatement.setString(7, flight.getAirport());
            callableStatement.setString(8, flight.getTo());
            callableStatement.setString(9, flight.getDate());
            callableStatement.setInt(10, flight.getNumOfPassenger());
            callableStatement.setInt(11, flight.getNumOfChildern());
            callableStatement.setInt(12, flight.getNumOfBaby());
            callableStatement.setDouble(13, flight.getTotalPrice());
            callableStatement.setString(14, flight.getNotes());
            
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "Success";
    }
    
    
    
    
    public static FlightBooking getFlightBooking(int ID) {
        FlightBooking flight =new FlightBooking();
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call getFlight(?)}");
            callableStatement.setInt(1, ID);
            ResultSet resultSet=callableStatement.executeQuery();
            if (resultSet.next()){                
               flight =new FlightBooking(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8), resultSet.getString(9), resultSet.getInt(10), resultSet.getInt(11), resultSet.getInt(12), resultSet.getDouble(13),  resultSet.getString(14));                                                                                         
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flight;
    }
    
    
    public static String deleteFlightBookingt(int ID) {
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call deleteBooking(?)}");
            callableStatement.setInt(1, ID);
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Success";
    }
    
    
    public static ArrayList<FlightBooking> getFlightBookings() {
        ArrayList<FlightBooking> flightBookings = new ArrayList<FlightBooking>();
        try {
            Connection connection = db.getConnection();
            CallableStatement callableStatement = connection.prepareCall("{call getFlightBookings()}");
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {
                FlightBooking flight =new FlightBooking(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8), resultSet.getString(9), resultSet.getInt(10), resultSet.getInt(11), resultSet.getInt(12), resultSet.getDouble(13),  resultSet.getString(14));                   
                flightBookings.add(flight);
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        return flightBookings;
    }
    
}
