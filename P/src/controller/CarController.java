package controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Car;

/**
 *
 * @author Giurgiu Carmen
 */
public class CarController {
    private static DBController db = new DBController();
    
    public static ArrayList<Car> getAllCars() {
        ArrayList<Car> cars = new ArrayList<Car>();
        try {
            Connection connection = db.getConnection();
            CallableStatement callableStatement = connection.prepareCall("{call getAllCars()}");
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {                
                Car car = new Car(resultSet.getInt(1),resultSet.getString(2), resultSet.getDouble(3));                                                                                        
                cars.add(car);
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        return cars;
    }
    
    
    
    public static String addCar(Car car){
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call addCar(?,?,?)}");
            callableStatement.setInt(1, car.getId());            
            callableStatement.setString(2, car.getCarType());
            callableStatement.setDouble(4, car.getPricePerday());
                        
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "Success";
    }
    
    
    
    public static String deleteCar(String type) {
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call deleteCar(?)}");
            callableStatement.setString(1, type);
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Success";  }
    
    
    
    
}
