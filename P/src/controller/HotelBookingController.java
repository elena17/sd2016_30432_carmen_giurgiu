package controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.HotelBooking;

/**
 *
 * @author Giurgiu Carmen
 */
public class HotelBookingController {
    private static DBController db = new DBController();
    
    public static String addHotelBooking(HotelBooking hotel){
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call addHotelBooking(?,?,?,?,?,?,?)}");
            callableStatement.setInt(1, hotel.getId());
            callableStatement.setInt(2, hotel.getTicketId());
            callableStatement.setInt(3, hotel.getHotelId());            
            callableStatement.setString(4, hotel.getCountry());
            callableStatement.setInt(5, hotel.getNoOfRooms());
            callableStatement.setInt(6, hotel.getNoOfNights());
            callableStatement.setDouble(6, hotel.getTotalPrice());
            
            
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "Success";
    }
    
    
    
    
    public static HotelBooking getHotelBooking(int ID) {
        HotelBooking hotel =new HotelBooking();
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call getHotelBooking(?)}");
            callableStatement.setInt(1, ID);
            ResultSet resultSet=callableStatement.executeQuery();
            if (resultSet.next()){                
               hotel =new HotelBooking(resultSet.getInt(1), resultSet.getInt(2),resultSet.getInt(3), resultSet.getString(4), resultSet.getInt(5), resultSet.getInt(6),resultSet.getDouble(7));                                                                                         
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hotel;
    }
    
    
    public static String deleteHotelBooking(int ID) {
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call deleteHotelBooking(?)}");
            callableStatement.setInt(1, ID);
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Success";
    }
    
    
    public static ArrayList<HotelBooking> getHotelBookings() {
        ArrayList<HotelBooking> hotelBookings = new ArrayList<HotelBooking>();
        try {
            Connection connection = db.getConnection();
            CallableStatement callableStatement = connection.prepareCall("{call getHotelBookings()}");
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {
                HotelBooking hotel =new HotelBooking(resultSet.getInt(1), resultSet.getInt(2),resultSet.getInt(3), resultSet.getString(4), resultSet.getInt(5), resultSet.getInt(6),resultSet.getDouble(7));                 
                hotelBookings.add(hotel);
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        return hotelBookings;
    }
}
