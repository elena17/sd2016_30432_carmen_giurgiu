package controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import model.Airport;

/**
 *
 * @author Giurgiu Carmen
 */
public class AirportController {
    private static DBController db = new DBController();
    
    public static String addAirport(Airport airport){
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call addAirport(?,?)}");
            callableStatement.setInt(1, airport.getId());
            callableStatement.setString(2, airport.getAirportName());
            
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "Success";
    }
    
    
    
    
    public static Airport getAirport(int ID) {
        Airport airport=new Airport();
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call getAirport(?)}");
            callableStatement.setInt(1, ID);
            ResultSet resultSet=callableStatement.executeQuery();
            if (resultSet.next()){                
                airport=new Airport(resultSet.getInt(1),resultSet.getString(2));
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return airport;
    }
    
    
    public static String deleteAirport(int ID) {
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call deleteAirport(?)}");
            callableStatement.setInt(1, ID);
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Success";
    }
    
    
    public static ArrayList<Airport> getAirports() {
        ArrayList<Airport> airports = new ArrayList<Airport>();
        try {
            Connection connection = db.getConnection();
            CallableStatement callableStatement = connection.prepareCall("{call getAirports()}");
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {
                Airport airport=new Airport(resultSet.getInt(1),resultSet.getString(2));
                airports.add(airport);
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        return airports;
    }
    
    
    public static void main(String [] args){
        /*
        Airport a = new Airport();
        a.setId(3);
        a.setAirportName("Avram Iancu");
        AirportController.addAirport(a);
        */
        
         //Airport a = AirportController.getAirport(3);
         
         //System.out.println("Id " + a.getId() + "   Name: " + a.getAirportName());
        
        AirportController.deleteAirport(4);
    }
}
