package controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Hotel;

/**
 *
 * @author Giurgiu Carmen
 */
public class HotelController {
    private static DBController db = new DBController();
    
    public static ArrayList<Hotel> getAllHotels() {
        ArrayList<Hotel> hotels = new ArrayList<Hotel>();
        try {
            Connection connection = db.getConnection();
            CallableStatement callableStatement = connection.prepareCall("{call getAllHotels()}");
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {                
                Hotel hotel = new Hotel(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDouble(4),resultSet.getString(5), resultSet.getString(6));                                                                                        
                hotels.add(hotel);
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        return hotels;
    }
    
    
    public static String addHotel(Hotel hotel){
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call addHotel(?,?,?,?,?,?)}");
            callableStatement.setInt(1, hotel.getId());
            callableStatement.setInt(2, hotel.getCountryId());
            callableStatement.setString(3, hotel.getHotelName());
            callableStatement.setDouble(4, hotel.getPriceAtNight());
            callableStatement.setString(5, hotel.getPhone());
            callableStatement.setString(6, hotel.getAddress());
            
            
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "Success";
    }
    
    
    
    
    public static Hotel getHotel(int ID) {
        Hotel hotel =new Hotel();
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call getHotel(?)}");
            callableStatement.setInt(1, ID);
            ResultSet resultSet=callableStatement.executeQuery();
            if (resultSet.next()){                
               hotel =new Hotel(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDouble(4), resultSet.getString(5), resultSet.getString(6));                                                                                         
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hotel;
    }
    
    
        public static double getHotelPrice(String name) {
        Hotel hotel =new Hotel();
        double price  = 0;
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call getHotelPrice(?)}");
            callableStatement.setString(1, name);
            ResultSet resultSet=callableStatement.executeQuery();
            if (resultSet.next()){                
               hotel =new Hotel(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDouble(4), resultSet.getString(5), resultSet.getString(6));                                                                                         
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        price = hotel.getPriceAtNight();
        
        return price;
    }
        
        
         public static int getHotelId(String name) {
        Hotel hotel =new Hotel();
        int id  = 0;
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call getHotelPrice(?)}");
            callableStatement.setString(1, name);
            ResultSet resultSet=callableStatement.executeQuery();
            if (resultSet.next()){                
               hotel =new Hotel(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDouble(4), resultSet.getString(5), resultSet.getString(6));                                                                                         
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        id = hotel.getId();
        
        return id;
    }
    
    
    public static String deleteHotel(String name) {
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call deleteHotel(?)}");
            callableStatement.setString(1, name);
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Success";
    }
    
    
    public static void main(String [] args){
        double price = 0;
        price = HotelController.getHotelPrice("Belvedere");
        
        System.out.println("Price at night: " + price);
    }
    
}
