package controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.CarBooking;

/**
 *
 * @author Giurgiu Carmen
 */
public class CarBookingController {
    private static DBController db = new DBController();
    
    public static ArrayList<CarBooking> getAllCars() {
        ArrayList<CarBooking> cars = new ArrayList<CarBooking>();
        try {
            Connection connection = db.getConnection();
            CallableStatement callableStatement = connection.prepareCall("{call getAllCarsBookings()}");
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {                
                CarBooking car = new CarBooking(resultSet.getInt(1), resultSet.getInt(2),resultSet.getString(3), resultSet.getInt(4),resultSet.getDouble(5));                                                                                        
                cars.add(car);
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        return cars;
    }
    
    
    
    public static String addCarBooking(CarBooking car){
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call addHotel(?,?,?,?,?)}");
            callableStatement.setInt(1, car.getId()); 
            callableStatement.setInt(2, car.getTicketId());
            callableStatement.setString(3, car.getCarType());
            callableStatement.setInt(4, car.getNoOfNights()); 
            callableStatement.setDouble(5, car.getTotalPrice());
                        
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "Success";
    }
    
    
    
    public static String deleteCarBooking(int id) {
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call deleteCarBooking(?)}");
            callableStatement.setInt(1, id);
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Success";
    }
    
    
     public static ArrayList<CarBooking> getCarBookings() {
        ArrayList<CarBooking> carBookings = new ArrayList<CarBooking>();
        try {
            Connection connection = db.getConnection();
            CallableStatement callableStatement = connection.prepareCall("{call getCarBookings()}");
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {
                CarBooking carBooking=new CarBooking(resultSet.getInt(1), resultSet.getInt(2),resultSet.getString(3), resultSet.getInt(4),resultSet.getDouble(5));
                carBookings.add(carBooking);
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        return carBookings;
    }
    
}
