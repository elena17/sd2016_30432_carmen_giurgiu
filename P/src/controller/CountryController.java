package controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Country;

/**
 *
 * @author Giurgiu Carmen
 */
public class CountryController {
     private static DBController db = new DBController();
    
     
    public static ArrayList<Country> getAllCountries() {
        ArrayList<Country> countries = new ArrayList<Country>();
        try {
            Connection connection = db.getConnection();
            CallableStatement callableStatement = connection.prepareCall("{call getAllCountries()}");
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {                
                Country country = new Country(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4),resultSet.getInt(5), resultSet.getInt(6), resultSet.getInt(7), resultSet.getInt(8));                                                                                        
                countries.add(country);
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        return countries;
    }
     
     
     
    public static String addCountry(Country country){
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call addCountry(?,?,?,?,?,?,?,?)}");
            callableStatement.setInt(1, country.getId());
            callableStatement.setInt(2, country.getId());
            callableStatement.setInt(3, country.getPriceOnGoPass());
            callableStatement.setInt(4, country.getPriceOnGoChildren());
            callableStatement.setInt(5, country.getPriceOnGoBaby());
            callableStatement.setInt(6, country.getPriceOnGoBackPass());
            callableStatement.setInt(7, country.getPriceOnGoBackChildren());
            callableStatement.setInt(8, country.getPriceOnGoBackBaby());
            
            
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "Success";
    }
    
    
    
    
    public static Country getCountry(int ID) {
        Country country =new Country();
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call getCountry(?)}");
            callableStatement.setInt(1, ID);
            ResultSet resultSet=callableStatement.executeQuery();
            if (resultSet.next()){                
               country =new Country(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getInt(5), resultSet.getInt(6), resultSet.getInt(7), resultSet.getInt(8));                                                                                         
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return country;
    }
    
    
    
    public static int getCountryId(String name) {
        Country country =new Country();
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call getCountryId(?)}");
            callableStatement.setString(1, name);
            ResultSet resultSet=callableStatement.executeQuery();
            if (resultSet.next()){                
               country =new Country(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getInt(5), resultSet.getInt(6), resultSet.getInt(7), resultSet.getInt(8));                                                                                         
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        int id = country.getId();
        
        return id;
    }
    
    
    
    public static String deleteCountry(String name) {
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call deleteCountry(?)}");
            callableStatement.setString(1, name);
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Success";
    }
}
