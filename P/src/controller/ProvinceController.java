package controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Province;

/**
 *
 * @author Giurgiu Carmen
 */
public class ProvinceController {
    private static DBController db = new DBController();
    
    public static String addProvince(Province province){
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call addProvince(?,?,?)}");
            callableStatement.setInt(1, province.getId());
            callableStatement.setInt(2, province.getAiportId());
            callableStatement.setString(3, province.getName());
            
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "Success";
    }
    
    
    
    
    public static Province getProvince(int ID) {
        Province province=new Province();
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call getProvince(?)}");
            callableStatement.setInt(1, ID);
            ResultSet resultSet=callableStatement.executeQuery();
            if (resultSet.next()){                
                province =new Province(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3));
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return province;
    }
        
   
    
    public static String deleteProvince(int ID) {
        try {
            Connection connection=db.getConnection();
            CallableStatement callableStatement=connection.prepareCall("{call deleteProvince(?)}");
            callableStatement.setInt(1, ID);
            callableStatement.executeQuery();
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Success";
    }
    
    
    public static ArrayList<Province> getProvinces() {
        ArrayList<Province> provinces = new ArrayList<Province>();
        try {
            Connection connection = db.getConnection();
            CallableStatement callableStatement = connection.prepareCall("{call getProvinces()}");
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {
                Province province =new Province(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3));
                provinces.add(province);
            }
            callableStatement.close();
            connection.close();
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        return provinces;
    }
}
