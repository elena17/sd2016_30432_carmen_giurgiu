<%-- 
    Document   : search
    Created on : 09.05.2016, 01:35:38
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.4.2.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script src="js/searchValidation.js" type="text/javascript"></script>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="user_panel.jsp">User</a></li>
            </ul>
            
            
            
            <div id="gradient-separator"></div>
            <div id="content">

                <hr>
                <h1>Search for books:</h1>
                <hr>
                 </br> 
                    <form id="searcByCategory" name="searchByCategory" method="POST" action="SearchUserController?action=searchByCategory">            
                        Category: <input id="category" name="category" value="${category}">
                        <input id="submitCategorySearch" name="submit" type="submit" value="Search by Category">
                    </form>
                    </br>
                    <form id="searcByTitle" name="searchByTitle" method="POST" action="SearchUserController?action=searchByTitle">            
                        Title: <input id="bookName" name="bookName" value="${bookName}">
                        <input id="submitTitleSearch" name="submit" type="submit" value="Search by Title">
                    </form>
                        </br>
                    <form id="searcByAuthor" name="searchByAuthor" method="POST" action="SearchUserController?action=searchByAuthor">            
                        Author: <input id="bookAuthor" name="bookAuthor" value="${bookAuthor}">
                        <input id="submitAuthorSearch" name="submit" type="submit" value="Search by Author">
                    </form>
                     
                    <div id="buttonSpace" Style="padding-top: 50px;">
                        <a href="user_panel.jsp"><button id="buttonReturn" type="button">Return To User Panel</button></a>
                    </div>
                     </br> 
            </div>
            
            
            
            
            
               
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>