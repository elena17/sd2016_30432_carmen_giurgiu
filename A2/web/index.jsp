<%-- 
    Document   : index
    Created on : 03.05.2016, 12:59:53
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Book Store</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
                <header>
                    <img src = "images/dummyBanner.jpg" alt=""/>
                </header>
            
            
                <ul id ="nav">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="book_store.jsp">Store</a></li>
                    <li><a href="login.jsp">Log in</a></li>
                </ul>
            
                <div id="content">
                 <h1>Books For Dummies</h1>
                 <hr>
                 <p> Welcome to books for dummies. We have been in business for over 23 long years and have had such a successful business we figured why not bring a website. Thats right we are going to be
                     online and selling some of our top selling books. So please if your are interested visit our STORE if you are looking to purchase some of our books online. If you dont think that our prices are fair
                     then you are sorely mistaken because we offer the best prices around. Better than amazon and overstock.com. </p>

                 <p style="color:grey;text-decoration: underline;">Quick Fun Fact: In 1991 the first book for DUMMIES that was released was - DOS for Dummies.</p>

                </div>
                <div id="gradient-separator"></div>

                <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
                </footer>

            
        </div>    
    </body>
    
</html>
