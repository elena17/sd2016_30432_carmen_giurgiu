<%-- 
    Document   : book_records
    Created on : 03.05.2016, 18:29:29
    Author     : vaio
--%>
<%@page import="java.io.IOException"%>
<%@page import="org.xml.sax.SAXException"%>
<%@page import="javax.xml.parsers.ParserConfigurationException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Book"%>
<%@page import="view.BookXMLReader"%>
<%@page import="java.util.List"%>
<% BookXMLReader br = new BookXMLReader(); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Book Store</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="admin_panel.jsp">Admin</a></li>
            </ul>
            
            
            <div id="records-content">        
                  <table style="width: 100%">
                  <tr>
                        <th>ID</th>
                        <th>Book Name</th>
                        <th>Book Author</th>
                        <th>ISBN</th>
                        <th>Category</th>
                        <th>Image</th>
                        <th>Quantity</th>
                        <th>Price</th>                 
                  </tr>
                  
                  <%
                    List<Book> books =  br.getAllBooks();
                    for(int i = 0; i < books.size(); i++){
                        int id = books.get(i).getId();
                        String bookName = books.get(i).getBookName();
                        String bookAuthor = books.get(i).getBookAuthor();
                        String isbn = books.get(i).getBookIsbn();
                        String category = books.get(i).getCategory();
                        String image = books.get(i).getImage();
                        int quantity = books.get(i).getQuantity();
                        double price =  books.get(i).getPrice();             
              
                %>
              
              
              <tr>   
                      <td><%= id %></td>
                      <td><%= bookName %></td>
                      <td><%= bookAuthor %></td>
                      <td><%= isbn %></td>
                      <td><%= category %></td>
                      <td><%= image %></td>
                      <td><%= quantity %></td>
                      <td><%= price %></td>
                
               
               </tr> 
              
              <% 
                 }
              %>
                  
              
                
              </table>
              <div id="panelButtons">
                <a href="delete_book.jsp"><button id="deleteRecode" type="button" value="Delete Record">Delete Record</button></a>
                <a href="new_book.jsp"><button id="createRecord" type="button" value="Create New Record">Create Record</button></a>
                <a href="update_book.jsp"><button id="updateRecord" type="button" value="Update Record">Update Record</button></a>
              <a href="admin_panel.jsp"><button id="buttonReturn" type="button">Return To Admin Panel</button></a>
              </div>
              </div>
            
            
      
      
            <div id="gradient-separator"></div>
               
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div> 
    </body>
    
</html>
