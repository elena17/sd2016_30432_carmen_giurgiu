<%-- 
    Document   : order_submitted
    Created on : 11.05.2016, 18:11:57
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Book Store</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
                <header>
                    <img src = "images/dummyBanner.jpg" alt=""/>
                </header>
            
            
                <ul id ="nav">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="book_store.jsp">Store</a></li>
                    <li><a href="login.jsp">Log in</a></li>
                </ul>
            
                
                <div id="content">
                 <h1>Success!</h1>
                 <hr>
                 
                </div>
                <div id="gradient-separator"></div>

                <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
                </footer>

            
        </div>    
    </body>