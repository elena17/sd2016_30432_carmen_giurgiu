<%-- 
    Document   : error_page_search
    Created on : 11.05.2016, 22:37:09
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Book Store</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="user_panel.jsp">User</a></li>     
            </ul>
            
            
            
           
             <div id="gradient-separator"></div>
         <div id="content">
             <h1>ERROR!</h1>
             <hr>
             <p>&nbsp;</p>
             <ul>
                 <li>
                 ${errorMessage}
                 </li>
                 <li>
                 <a href="user_panel.jsp">Return To User Panel</a>  
                 </li>
             </ul>
             
         </div>
         <div id="gradient-separator"></div>
            
            
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>