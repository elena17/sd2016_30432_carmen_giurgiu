<%-- 
    Document   : update_book
    Created on : 09.05.2016, 02:43:44
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.4.2.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script src="js/bookFormsValidation.js" type="text/javascript"></script>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="admin_panel.jsp">Admin</a></li>
            </ul>
            
            
            
            
         <div id="gradient-separator"></div>
         <div id="content">
            <h1>Please Fill Out The Required Fields Below And Submit To Create A New Record</h1>
            <hr>
            <span>Search for a record you wish to update</span>
            <p></p>
            
             <form id="newBookForm" name="newBookForm" method="POST" action="BookAdminController?action=findRecordToUpdate">  
                  <p>ID: <input id="id" name="id" value="${id}"> <input id="findRecordToUpdate" name="submit" type="submit" value="Search For Record"></p>
             </form>
            <p></p>
            <hr>
            
            <form id="newBookForm" name="newBookForm" method="POST" action="BookAdminController?action=updateRecord">  
                <p> ID: <input id="id" name="id" value="${id}"></p>
                <p> Book Name: <input id="bookName" name="bookName" value="${bookName}"></p>
                <p> Author Name: <input id="bookAuthor" name="bookAuthor" value="${bookAuthor}"></p>
                <p> ISBN: <input id="bookIsbn" name="bookIsbn" value="${bookIsbn}"></p>
                <p> Category: <input id="category" name="category" value="${category}"></p>
                <p> Image(File Location): <input id="image" name="image" value="${image}"></p>
                <p> Quantity: <input id="quantity" name="quantity"  value="${quantity}"></p>
                <p> Price: <input id="price" name="price" value="${price}"></p>
                <p> <input id="submitUpdate" name="submit" type="submit" value="Update Record">
            </form>
        
            <div id="buttonSpace" Style="padding-bottom: 25px;">
                <a href="book_records.jsp"><button id="buttonReturn" type="button">Return To Book Records</button></a>
            </div>
        </div>           
            
                 
            
             
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>

