<%-- 
    Document   : admin_panel
    Created on : 04.05.2016, 02:26:02
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="admin_panel.jsp">Admin</a></li>
            </ul>
            
            <h1>Admin panel</h1>
            
            
            <div id="admin-content">           
                <a href="book_records.jsp"><button type="button" id="findRecords" value="Book Records" >Book Records</button></a>
                <a href="user_records.jsp"><button type="button" id="findRecords" value="Customer Records">User Records</button></a>
                <a href="reports.jsp"><button type="button" id="findRecords" value="Order Records">Generate Reports</button></a>
            </div>
            
            
               
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>
