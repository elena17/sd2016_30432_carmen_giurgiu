<%-- 
    Document   : book_store
    Created on : 03.05.2016, 21:40:48
    Author     : vaio
--%>

<%
    BookXMLReader bw = new BookXMLReader();
%>

<%@page import="model.Book"%>
<%@page import="java.util.List"%>
<%@page import="view.BookXMLReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.4.2.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script src="js/cartScript.js" type="text/javascript"></script>
        <title>Online Book Store</title>
    </head>
    <body>
   <div id="wrapper">
                    <header>
                        <img src = "images/dummyBanner.jpg" alt=""/>
                    </header>
       
       
                    <ul id="nav">
                       <li><a href="index.jsp">Home</a></li>           
                       <li><a href="book_store.jsp">Store</a></li>
                       <li><a href="login.jsp">Log in</a></li>     
                    </ul>    
       
            
                    <div id="gradient-separator"></div>
                    

                    <div id="page_title">
                        <hr>
                        <h1>Our Current Selections Available</h1>
                        <hr>
                    </div>
                    
                    
                    <form id="bookFrom" name="bookForm" method="POST" action="MainController">
                        <ul class="products">
                                <%
                                    //String bookCategory = "Programming";
                                    List<Book> books = bw.getAllBooks();
                                    String quantity = null;
                                    for(int i = 0; i < books.size(); i++ ){
                                        String bookList = books.get(i).getBookName() + " " + books.get(i).getPrice()+" RON";
                                        String image = "images/" + books.get(i).getImage() + ".jpg";
                                        if(books.get(i).getQuantity() > 0){
                                            quantity = ""+books.get(i).getQuantity()+ " in stock";   
                                        }else{
                                            quantity = "Out of stock";
                                        }
                                        
                                %>
                            
                                <li>
                                    <img alt="<%=image%>" src="<%=image%>" height="220" width="190"/>  
                                    <p></p>
                                    <div class="module"> 
                                    <input type="checkbox" name="itemSelected" value="<%= bookList %>"/><span class="bookName"><%= bookList %></span>
                                    </br>
                                    <span class="bookName"><%= quantity %></span>
                                    <p></p>
                                    </div>
                                </li>
                                
                                <%
                                }

                                %>
                            
                        </ul>

                        <div id="cssmenu">           

                                   <h3 align="center">Shopping Cart</h3>
                                   <ul class="cartItemsSelected">

                                   </ul>
                                   <ul> <li><input id="submitOrder" type="submit" value="Checkout"></li></ul>
                        </div>
                                
                                

                    </form>
      
            
                    <div id="gradient-separator"></div>
               
                    <footer>
                        <img src="images/dummiesSlider.jpg" alt=""/>
                    </footer>
	</div>    
        
    </body>
</html>