<%-- 
    Document   : new_user
    Created on : 09.05.2016, 03:52:01
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.4.2.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script src="js/userFormsValidation.js" type="text/javascript"></script>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="admin_panel.jsp">Admin</a></li>
            </ul>
            
            
            
            <div id="gradient-separator"></div>
        <div id="content">
        <h1>Please Fill Out The Required Fields Below And Submit To Create A New Record</h1>
        <hr>
        <span>If you notice you have made a mistake after submitting you can always update the record.</span>
        <p></p>
        
        <form id="newUserForm" name="newBookForm" method="POST" action="UserAdminController?action=createNewRecord">  
                <p> Id: <input id="id" name="id" value="${id}"></p>
                <p> Name: <input id="name" name="name" value="${name}"></p>
                <p> Username: <input id="username" name="username" value="${username}"></p>
                <p> Password: <input id="password" name="password" value="${password}"></p>
                <p> Aministrator Rights: <input id="isAdmin" name="isAdmin" value="${isAdmin}"></p>                
            <p> <input id="submitRecordUser" name="submit" type="submit" value="Create Record">
        </form>
        
        <div id="buttonSpace" Style="padding-bottom: 25px;">
            <a href="user_records.jsp"><button id="buttonReturn" type="button">Return To User Records</button></a>
        </div>
        </div>          
            
                 
            
            
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>