<%-- 
    Document   : order_records
    Created on : 09.05.2016, 01:37:10
    Author     : vaio
--%>
<%@page import="java.io.IOException"%>
<%@page import="org.xml.sax.SAXException"%>
<%@page import="javax.xml.parsers.ParserConfigurationException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Order"%>
<%@page import="view.OrderXMLReader"%>
<%@page import="java.util.List"%>
<% OrderXMLReader or = new OrderXMLReader(); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="user_panel.jsp">User</a></li>
            </ul>
            
           
            <div id="records-content">        
                  <table style="width: 100%">
                  <tr>
                        <th>ID</th>
                        <th>Customer Name</th>
                        <th>Order Number</th>
                        <th>Order Date</th>
                        <th>Total</th>
                        <th>Status</th>
                                       
                  </tr>
                  
                  <%
                List<Order> orders =  or.getAllOrders();
                for(int i = 0; i < orders.size(); i++){
                    int id = orders.get(i).getId();
                    String customerName = orders.get(i).getCustamerName();
                    String orderNumber = orders.get(i).getOrderNumber();
                    String orderDate = orders.get(i).getOrderDate();
                    double total = orders.get(i).getOrderTotal();
                    String status =  orders.get(i).getStatus();
              %>
              
              
              <tr>   
                      <td><%= id %></td>
                      <td><%= customerName %></td>
                      <td><%= orderNumber %></td>
                      <td><%= orderDate %></td>
                      <td><%= total %></td>
                      <td><%= status %></td>                      
                
               
               </tr> 
              
              <% 
                 }
              %>
                  
              
                
              </table>
              <div id="panelButtons">
                <a href="delete_order.jsp"><button id="deleteRecode" type="button" value="Delete Record">Delete Record</button></a>
                <a href="new_order.jsp"><button id="createRecord" type="button" value="Create New Record">Create Record</button></a>
                <a href="update_order.jsp"><button id="updateRecord" type="button" value="Update Record">Update Record</button></a>
              <a href="user_panel.jsp"><button id="buttonReturn" type="button">Return To User Panel</button></a>
              </div>
              </div>
            
            
            
            
            
               
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>