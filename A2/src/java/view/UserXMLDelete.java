package view;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author vaio
 */

public final class UserXMLDelete  {


    public void deleteUser(String idUser){
          try {
               
                SAXBuilder builder = new SAXBuilder();
                Document document = builder.build(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\users.xml"));

               
                Element rootElement = document.getRootElement();

                List<Element> users = rootElement.getChildren();                
                
                for (int i=0; i< users.size(); i++){
                    Element elem = users.get(i);
                    String s = elem.getChild("id").getText();
                    
                    if (s.equals(idUser)){
                        rootElement.getChildren().remove(i);
                    }
                }
               
                XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
                outputter.output(document, new FileOutputStream("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\users.xml"));
          }
          catch (Exception e) {
                e.printStackTrace();
          }
    }


    public static void main(String[] args){
          new UserXMLDelete ().deleteUser("12");
    }

}
