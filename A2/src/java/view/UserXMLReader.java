package view;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import model.User;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author vaio
 */
public class UserXMLReader {
    public ArrayList<User> getAllUsers()throws ParserConfigurationException, SAXException, IOException{
        ArrayList<User> users = new ArrayList<User>();
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\users.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String name = elem.getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue(); 
                String username = elem.getElementsByTagName("username").item(0).getChildNodes().item(0).getNodeValue(); 
                String password = elem.getElementsByTagName("password").item(0).getChildNodes().item(0).getNodeValue();                
                boolean isAdmin = Boolean.parseBoolean(elem.getElementsByTagName("isAdmin").item(0).getChildNodes().item(0).getNodeValue());
                
                users.add(new User(id, name, username, password, isAdmin));
            }            
            
        }
        
        return users;
    }
    
    public ArrayList<User> getAllNonAdminUsers()throws ParserConfigurationException, SAXException, IOException{
        ArrayList<User> users = new ArrayList<User>();
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\users.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String name = elem.getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue(); 
                String username = elem.getElementsByTagName("username").item(0).getChildNodes().item(0).getNodeValue(); 
                String password = elem.getElementsByTagName("password").item(0).getChildNodes().item(0).getNodeValue();                
                boolean isAdmin = Boolean.parseBoolean(elem.getElementsByTagName("isAdmin").item(0).getChildNodes().item(0).getNodeValue());
                
                if (!isAdmin)
                users.add(new User(id, name, username, password, isAdmin));
            }            
            
        }
        
        return users;
    }
    
    
    
    public User getUser(String us, String ps)throws ParserConfigurationException, SAXException, IOException{
        User user = null;
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\users.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String name = elem.getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue(); 
                String username = elem.getElementsByTagName("username").item(0).getChildNodes().item(0).getNodeValue(); 
                String password = elem.getElementsByTagName("password").item(0).getChildNodes().item(0).getNodeValue();                
                boolean isAdmin = Boolean.parseBoolean(elem.getElementsByTagName("isAdmin").item(0).getChildNodes().item(0).getNodeValue());
                
                if (username.equals(us) && password.equals(ps))
                    user = new User(id, name, username, password, isAdmin);
            }            
            
        }
        
        return user;
    }
    
    
     public User getUserById(int idUser)throws ParserConfigurationException, SAXException, IOException{
        User user = null;
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\users.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String name = elem.getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue(); 
                String username = elem.getElementsByTagName("username").item(0).getChildNodes().item(0).getNodeValue(); 
                String password = elem.getElementsByTagName("password").item(0).getChildNodes().item(0).getNodeValue();                
                boolean isAdmin = Boolean.parseBoolean(elem.getElementsByTagName("isAdmin").item(0).getChildNodes().item(0).getNodeValue());
                
                if (id == idUser)
                    user = new User(id, name, username, password, isAdmin);
            }            
            
        }
        
        return user;
    }
    
    
    
    
     public static void main(String[] args)  {

        UserXMLReader ex= new UserXMLReader();
  /*      
        try{
            ArrayList<User> users = ex.getAllUsers();
          for (User user : users)

               System.out.println(user.toString());
        }catch(ParserConfigurationException e){
            e.printStackTrace();
        }catch(SAXException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
  */
  /*     try{
           User user = ex.getUser("elena", "pass");
          System.out.println(user);
               
        }catch(ParserConfigurationException e){
            e.printStackTrace();
        }catch(SAXException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
*/
        try{
           User user = ex.getUserById(11);
          System.out.println(user);
               
        }catch(ParserConfigurationException e){
            e.printStackTrace();
        }catch(SAXException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        
     }
}