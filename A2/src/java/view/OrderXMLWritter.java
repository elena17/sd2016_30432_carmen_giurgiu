/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author vaio
 */
public class OrderXMLWritter {
    public void addOrder(String id, String customerName, String orderNumber, String orderDate, String total, String status)
                        throws ParserConfigurationException, TransformerConfigurationException, TransformerException, SAXException, IOException{
       File xml = new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\orders.xml");
       
       DocumentBuilderFactory dbf =  DocumentBuilderFactory.newInstance();
       DocumentBuilder db = dbf.newDocumentBuilder();
       Document doc = db.parse(xml);
       
       Element root = doc.getDocumentElement();
       Element order = doc.createElement("order");
       root.appendChild(order);
       
       Element idOrder = doc.createElement("id");
       Element nameOfCustomer = doc.createElement("customerName");
       Element orderNr = doc.createElement("orderNumber");
       Element orderDt = doc.createElement("orderDate");
       Element tot = doc.createElement("total");
       Element st = doc.createElement("status");
       
       
       idOrder.appendChild(doc.createTextNode(id));
       nameOfCustomer.appendChild(doc.createTextNode(customerName));
       orderNr.appendChild(doc.createTextNode(orderNumber));
       orderDt.appendChild(doc.createTextNode(orderDate));
       tot.appendChild(doc.createTextNode(total));
       st.appendChild(doc.createTextNode(status));
       
       
       order.appendChild(idOrder);
       order.appendChild(nameOfCustomer);
       order.appendChild(orderNr);
       order.appendChild(orderDt);
       order.appendChild(tot);
       order.appendChild(st);
       
       TransformerFactory factory = TransformerFactory.newInstance();
       Transformer transformer = factory.newTransformer();
       transformer.setOutputProperty(OutputKeys.INDENT, "yes");
       DOMSource domSource = new DOMSource(doc);
       StreamResult streamResult = new StreamResult(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\orders.xml"));
       transformer.transform(domSource, streamResult);

       DOMSource source = new DOMSource(doc);
       
    }
    
    public static void main(String [] args){
            OrderXMLWritter  or = new OrderXMLWritter ();
            try{
               or.addOrder("1", "Giurgiu Carmen", "3200", "11-05-2016", "16.99", "pending");
            }catch(IOException e){
                e.printStackTrace();
            }catch (ParserConfigurationException e){
                e.printStackTrace();
            }catch(TransformerConfigurationException e){
                e.printStackTrace();;
            }catch(TransformerException e){
                e.printStackTrace();
            }catch(SAXException e){
                e.printStackTrace();
            }
            
        }
}
