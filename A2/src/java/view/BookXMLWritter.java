package view;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author Giurgiu Carmen
 */
public class BookXMLWritter {
    public void addBook(String id, String bookName, String bookAuthor, String bookIsbn, String category, String image, String quantity, String price)
                        throws ParserConfigurationException, TransformerConfigurationException, TransformerException, SAXException, IOException{
       File xml = new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\books.xml");
       
       DocumentBuilderFactory dbf =  DocumentBuilderFactory.newInstance();
       DocumentBuilder db = dbf.newDocumentBuilder();
       Document doc = db.parse(xml);
       
       Element root = doc.getDocumentElement();
       Element book = doc.createElement("book");
       root.appendChild(book);
       
       Element idBook = doc.createElement("id");
       Element name = doc.createElement("bookName");
       Element author = doc.createElement("bookAuthor");
       Element isbn = doc.createElement("bookIsbn");
       Element categ = doc.createElement("category");
       Element img = doc.createElement("image");
       Element quant = doc.createElement("quantity");
       Element bookPrice = doc.createElement("price");
       
       idBook.appendChild(doc.createTextNode(id));
       name.appendChild(doc.createTextNode(bookName));
       author.appendChild(doc.createTextNode(bookAuthor));
       isbn.appendChild(doc.createTextNode(bookIsbn));
       categ.appendChild(doc.createTextNode(category));
       img.appendChild(doc.createTextNode(image));
       quant.appendChild(doc.createTextNode(quantity));
       bookPrice.appendChild(doc.createTextNode(price));
       
       book.appendChild(idBook);
       book.appendChild(name);
       book.appendChild(author);
       book.appendChild(isbn);
       book.appendChild(categ);
       book.appendChild(img);
       book.appendChild(quant);
       book.appendChild(bookPrice);
       
       TransformerFactory factory = TransformerFactory.newInstance();
       Transformer transformer = factory.newTransformer();
       transformer.setOutputProperty(OutputKeys.INDENT, "yes");
       DOMSource domSource = new DOMSource(doc);
       StreamResult streamResult = new StreamResult(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\books.xml"));
       transformer.transform(domSource, streamResult);

       DOMSource source = new DOMSource(doc);
       
    }
    
    
    public static void main(String [] args){
            BookXMLWritter  br = new BookXMLWritter ();
            try{
               br.addBook("2", "ASP for Dummies", "Jim Hardy", "184-448494-484", "asp", "asp-dummies", "7", "18.99"); 
            }catch(IOException e){
                e.printStackTrace();
            }catch (ParserConfigurationException e){
                e.printStackTrace();
            }catch(TransformerConfigurationException e){
                e.printStackTrace();;
            }catch(TransformerException e){
                e.printStackTrace();
            }catch(SAXException e){
                e.printStackTrace();
            }
            
        }
   
}
