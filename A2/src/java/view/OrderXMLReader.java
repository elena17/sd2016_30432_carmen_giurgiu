package view;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import model.Order;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author vaio
 */
public class OrderXMLReader {
    public ArrayList<Order> getAllOrders()throws ParserConfigurationException, SAXException, IOException{
        ArrayList<Order> orders = new ArrayList<Order>();
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\orders.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String name = elem.getElementsByTagName("customerName").item(0).getChildNodes().item(0).getNodeValue(); 
                String number = elem.getElementsByTagName("orderNumber").item(0).getChildNodes().item(0).getNodeValue();
                String date = elem.getElementsByTagName("orderDate").item(0).getChildNodes().item(0).getNodeValue(); 
                double total = Double.parseDouble(elem.getElementsByTagName("total").item(0).getChildNodes().item(0).getNodeValue());
                String status = elem.getElementsByTagName("status").item(0).getChildNodes().item(0).getNodeValue();                
                
                orders.add(new Order(id, name, number, date, total, status));
            }            
            
        }
        
        return orders;
    }
    
    public Order getOrderById(int idOrder)throws ParserConfigurationException, SAXException, IOException{
        Order order = null;
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\orders.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String name = elem.getElementsByTagName("customerName").item(0).getChildNodes().item(0).getNodeValue(); 
                String number = elem.getElementsByTagName("orderNumber").item(0).getChildNodes().item(0).getNodeValue();
                String date = elem.getElementsByTagName("orderDate").item(0).getChildNodes().item(0).getNodeValue(); 
                double total = Double.parseDouble(elem.getElementsByTagName("total").item(0).getChildNodes().item(0).getNodeValue());
                String status = elem.getElementsByTagName("status").item(0).getChildNodes().item(0).getNodeValue();    
                
                if (id == idOrder)
                    order = new Order(id, name, number, date, total, status);
            }            
            
        }
        
        return order;
    }
    
    
    
    
     public static void main(String[] args)  {

        OrderXMLReader ex= new OrderXMLReader();
  
        try{
           Order order = ex.getOrderById(1);
          System.out.println(order);
               
        }catch(ParserConfigurationException e){
            e.printStackTrace();
        }catch(SAXException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        
     }
}
