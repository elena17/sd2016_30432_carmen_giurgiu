package view;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author vaio
 */
public class UserXMLWritter {
    public void addUser(String id, String name, String username, String password, String isAdmin)
                        throws ParserConfigurationException, TransformerConfigurationException, TransformerException, SAXException, IOException{
       File xml = new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\users.xml");
       
       DocumentBuilderFactory dbf =  DocumentBuilderFactory.newInstance();
       DocumentBuilder db = dbf.newDocumentBuilder();
       Document doc = db.parse(xml);
       
       Element root = doc.getDocumentElement();
       Element user = doc.createElement("user");
       root.appendChild(user);
       
       Element idUser = doc.createElement("id");
       Element nameOfUser = doc.createElement("name");
       Element us = doc.createElement("username");
       Element ps = doc.createElement("password");
       Element isAdministrator = doc.createElement("isAdmin");
       
       
       idUser.appendChild(doc.createTextNode(id));
       nameOfUser.appendChild(doc.createTextNode(name));
       us.appendChild(doc.createTextNode(username));
       ps.appendChild(doc.createTextNode(password));
       isAdministrator.appendChild(doc.createTextNode(isAdmin));
       
       
       user.appendChild(idUser);
       user.appendChild(nameOfUser);
       user.appendChild(us);
       user.appendChild(ps);
       user.appendChild(isAdministrator);
       
       TransformerFactory factory = TransformerFactory.newInstance();
       Transformer transformer = factory.newTransformer();
       transformer.setOutputProperty(OutputKeys.INDENT, "yes");
       DOMSource domSource = new DOMSource(doc);
       StreamResult streamResult = new StreamResult(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\users.xml"));
       transformer.transform(domSource, streamResult);

       DOMSource source = new DOMSource(doc);
       
    }
    
 
    public static void main(String [] args){
            UserXMLWritter  ur = new UserXMLWritter ();
            try{
               ur.addUser("11", "Stan Ciprian", "cipri", "stan01", "false");
            }catch(IOException e){
                e.printStackTrace();
            }catch (ParserConfigurationException e){
                e.printStackTrace();
            }catch(TransformerConfigurationException e){
                e.printStackTrace();;
            }catch(TransformerException e){
                e.printStackTrace();
            }catch(SAXException e){
                e.printStackTrace();
            }
            
        }

    
}
