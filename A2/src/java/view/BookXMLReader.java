package view;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import model.Book;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author vaio
 */
public class BookXMLReader {
    public ArrayList<Book> getAllBooks()throws ParserConfigurationException, SAXException, IOException{
        ArrayList<Book> books = new ArrayList<Book>();
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\books.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String bookName = elem.getElementsByTagName("bookName").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookAuthor = elem.getElementsByTagName("bookAuthor").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookIsbn = elem.getElementsByTagName("bookIsbn").item(0).getChildNodes().item(0).getNodeValue(); 
                String category = elem.getElementsByTagName("category").item(0).getChildNodes().item(0).getNodeValue(); 
                String image = elem.getElementsByTagName("image").item(0).getChildNodes().item(0).getNodeValue();
                Integer quantity = Integer.parseInt(elem.getElementsByTagName("quantity").item(0).getChildNodes().item(0).getNodeValue());
                Double price = Double.parseDouble(elem.getElementsByTagName("price").item(0).getChildNodes().item(0).getNodeValue());
                
                books.add(new Book(id, bookName, bookAuthor, bookIsbn, category, image, quantity, price));
            }            
            
        }
        
        return books;
     }
    
    
    public ArrayList<Book> getBooksOutOfStock()throws ParserConfigurationException, SAXException, IOException{
        ArrayList<Book> books = new ArrayList<Book>();
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\books.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String bookName = elem.getElementsByTagName("bookName").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookAuthor = elem.getElementsByTagName("bookAuthor").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookIsbn = elem.getElementsByTagName("bookIsbn").item(0).getChildNodes().item(0).getNodeValue(); 
                String category = elem.getElementsByTagName("category").item(0).getChildNodes().item(0).getNodeValue(); 
                String image = elem.getElementsByTagName("image").item(0).getChildNodes().item(0).getNodeValue();
                Integer quantity = Integer.parseInt(elem.getElementsByTagName("quantity").item(0).getChildNodes().item(0).getNodeValue());
                Double price = Double.parseDouble(elem.getElementsByTagName("price").item(0).getChildNodes().item(0).getNodeValue());
                
                if (quantity == 0){
                    books.add(new Book(id, bookName, bookAuthor, bookIsbn, category, image, quantity, price));
                }
            }            
            
        }
        
        return books;
     }
    
    
    
    public List<Book> getBookByTitle(String title)throws ParserConfigurationException, SAXException, IOException{
        List<Book> books = new ArrayList<Book>();
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\books.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String bookName = elem.getElementsByTagName("bookName").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookAuthor = elem.getElementsByTagName("bookAuthor").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookIsbn = elem.getElementsByTagName("bookIsbn").item(0).getChildNodes().item(0).getNodeValue(); 
                String category = elem.getElementsByTagName("category").item(0).getChildNodes().item(0).getNodeValue(); 
                String image = elem.getElementsByTagName("image").item(0).getChildNodes().item(0).getNodeValue();
                Integer quantity = Integer.parseInt(elem.getElementsByTagName("quantity").item(0).getChildNodes().item(0).getNodeValue());
                Double price = Double.parseDouble(elem.getElementsByTagName("price").item(0).getChildNodes().item(0).getNodeValue());
                
                if (bookName.equals(title))
                books.add(new Book(id, bookName, bookAuthor, bookIsbn, category, image, quantity, price));
            }            
            
        }
        
        return books;
     }
    
    
    
    
    public List<Book> getBookByAuthor(String author)throws ParserConfigurationException, SAXException, IOException{
        List<Book> books = new ArrayList<Book>();
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\books.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String bookName = elem.getElementsByTagName("bookName").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookAuthor = elem.getElementsByTagName("bookAuthor").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookIsbn = elem.getElementsByTagName("bookIsbn").item(0).getChildNodes().item(0).getNodeValue(); 
                String category = elem.getElementsByTagName("category").item(0).getChildNodes().item(0).getNodeValue(); 
                String image = elem.getElementsByTagName("image").item(0).getChildNodes().item(0).getNodeValue();
                Integer quantity = Integer.parseInt(elem.getElementsByTagName("quantity").item(0).getChildNodes().item(0).getNodeValue());
                Double price = Double.parseDouble(elem.getElementsByTagName("price").item(0).getChildNodes().item(0).getNodeValue());
                
                if (bookAuthor.equals(author))
                books.add( new Book(id, bookName, bookAuthor, bookIsbn, category, image, quantity, price));
            }            
            
        }
        
        return books;
     }
    
  
    
    
    public List<Book> getBookByCategory(String categ)throws ParserConfigurationException, SAXException, IOException{
        List<Book> books = new ArrayList<Book>();
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\books.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String bookName = elem.getElementsByTagName("bookName").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookAuthor = elem.getElementsByTagName("bookAuthor").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookIsbn = elem.getElementsByTagName("bookIsbn").item(0).getChildNodes().item(0).getNodeValue(); 
                String category = elem.getElementsByTagName("category").item(0).getChildNodes().item(0).getNodeValue(); 
                String image = elem.getElementsByTagName("image").item(0).getChildNodes().item(0).getNodeValue();
                Integer quantity = Integer.parseInt(elem.getElementsByTagName("quantity").item(0).getChildNodes().item(0).getNodeValue());
                Double price = Double.parseDouble(elem.getElementsByTagName("price").item(0).getChildNodes().item(0).getNodeValue());
                
                if (category.equals(categ))
                books.add(new Book(id, bookName, bookAuthor, bookIsbn, category, image, quantity, price));
            }            
            
        }
        
        return books;
     }
    
    
    public Book getBookById(int idBook)throws ParserConfigurationException, SAXException, IOException{
        Book book = null;
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\books.xml"));
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i); 
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;  
                
                Integer id = Integer.parseInt(elem.getElementsByTagName("id").item(0).getChildNodes().item(0).getNodeValue());
                String bookName = elem.getElementsByTagName("bookName").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookAuthor = elem.getElementsByTagName("bookAuthor").item(0).getChildNodes().item(0).getNodeValue(); 
                String bookIsbn = elem.getElementsByTagName("bookIsbn").item(0).getChildNodes().item(0).getNodeValue(); 
                String category = elem.getElementsByTagName("category").item(0).getChildNodes().item(0).getNodeValue(); 
                String image = elem.getElementsByTagName("image").item(0).getChildNodes().item(0).getNodeValue();
                Integer quantity = Integer.parseInt(elem.getElementsByTagName("quantity").item(0).getChildNodes().item(0).getNodeValue());
                Double price = Double.parseDouble(elem.getElementsByTagName("price").item(0).getChildNodes().item(0).getNodeValue());
                
                if (id == idBook)
                book = new Book(id, bookName, bookAuthor, bookIsbn, category, image, quantity, price);
            }            
            
        }
        
        return book;
     }
    
    
    public static void main(String[] args)  {

        BookXMLReader ex= new BookXMLReader();

        try{
            ArrayList<Book> books = ex.getBooksOutOfStock();
          for (Book book : books)

               System.out.println(book.toString());
        }catch(ParserConfigurationException e){
            e.printStackTrace();
        }catch(SAXException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

        /*
        try{
            Book book = ex.getBookById(1);
                    System.out.println(book);
        }catch(ParserConfigurationException e){
            e.printStackTrace();
        }catch(SAXException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        */
     }  
    
}
