package view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author vaio
 */
public class BookXMLUpdate {
    public void updateBook(String id, String bookName, String bookAuthor, String bookIsbn, String category, String image, String quantity, String price) throws JDOMException, IOException{
        org.jdom2.Document doc = useSAXParser("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\books.xml");
    
         
        Element rootElement = doc.getRootElement();
        List<Element> listBooks= rootElement.getChildren("book");
        
        
        for (Element elem : listBooks) {
            String idBook = elem.getChildText("id");
            
            if (idBook.equals(id)){
                String title = elem.getChildText("bookName");
                if (title != null)
                    elem.getChild("bookName").setText(bookName);
                
                String author = elem.getChildText("bookAuthor");
                if (author != null)
                    elem.getChild("bookAuthor").setText(bookAuthor);
                
                String isbn = elem.getChildText("bookIsbn");
                if (isbn != null)
                    elem.getChild("bookIsbn").setText(bookIsbn);
                
                
                String categ = elem.getChildText("category");
                if (categ != null)
                    elem.getChild("category").setText(category);
                
                String img = elem.getChildText("image");
                if (img != null)
                    elem.getChild("image").setText(image);
                
                String quant = elem.getChildText("quantity");
                if (quant != null)
                    elem.getChild("quantity").setText(quantity);
                
                String pr = elem.getChildText("price");
                if (pr != null)
                    elem.getChild("price").setText(price);
                
            }          
        }
        
        
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());        
        xmlOutputter.output(doc, new FileOutputStream("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\books.xml"));
    }
    
    
    
    private static org.jdom2.Document useSAXParser(String fileName) throws JDOMException,
            IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        return saxBuilder.build(new File(fileName));
    }
    
 public static void main (String [] atgs){
     BookXMLUpdate bu = new BookXMLUpdate();
     
     
     try{
            bu.updateBook("2", "ASP for Dummies", "Jim Hardy", "184-448494-484", "asp", "asp-dummies", "7", "18.99");
        }catch(JDOMException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
 }
}
