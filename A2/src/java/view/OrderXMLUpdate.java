package view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author vaio
 */
public class OrderXMLUpdate {
    public void updateOrder(String id, String customerName, String orderNumber, String orderDate, String total, String status) throws JDOMException, IOException{
        org.jdom2.Document doc = useSAXParser("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\orders.xml");
    
         
        Element rootElement = doc.getRootElement();
        List<Element> listOrders= rootElement.getChildren("order");
        
        
        for (Element elem : listOrders) {
            String idOrder = elem.getChildText("id");
            
            if (idOrder.equals(id)){
                String st = elem.getChildText("status");
                if (st != null)
                    elem.getChild("status").setText(status);
                
                
            }          
        }
        
        
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());        
        xmlOutputter.output(doc, new FileOutputStream("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\orders.xml"));
    }
    
    
    
    private static org.jdom2.Document useSAXParser(String fileName) throws JDOMException,
            IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        return saxBuilder.build(new File(fileName));
    }
    
 
    
  
    
    
    
    public static void main (String [] args){
        OrderXMLUpdate ou = new OrderXMLUpdate();
       
        try{
            ou.updateOrder("3", null, null, null, null, "pending");
        }catch(JDOMException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
   
        
      
    }
}
