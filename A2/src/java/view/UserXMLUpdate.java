package view;

/**
 *
 * @author vaio
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom2.Element;
import org.jdom2.JDOMException;

import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


/**
 *
 * @author vaio
 */
public class UserXMLUpdate { 
    
    public void updateUser(String id, String name, String username, String password, String isAdmin) throws JDOMException, IOException{
        org.jdom2.Document doc = useSAXParser("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\users.xml");
    
         
        Element rootElement = doc.getRootElement();
        List<Element> listUsers= rootElement.getChildren("user");
        
        
        for (Element elem : listUsers) {
            String idUser = elem.getChildText("id");
            
            if (idUser.equals(id)){
                String nameOfuser = elem.getChildText("name");
                if (nameOfuser != null)
                    elem.getChild("name").setText(name);
                
                String us = elem.getChildText("username");
                if (us != null)
                    elem.getChild("username").setText(username);
                
                String ps = elem.getChildText("password");
                if (ps != null)
                    elem.getChild("password").setText(password);
                
                String isAdministrator = elem.getChildText("isAdmin");
                if (isAdministrator != null)
                    elem.getChild("isAdmin").setText(isAdmin);
            }          
        }
        
        
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());        
        xmlOutputter.output(doc, new FileOutputStream("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\web\\files\\users.xml"));
    }
    
    
    
    private static org.jdom2.Document useSAXParser(String fileName) throws JDOMException,
            IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        return saxBuilder.build(new File(fileName));
    }
    
 
    
  
    
    
    
    public static void main (String [] args){
        UserXMLUpdate ua = new UserXMLUpdate();
       
        try{
            ua.updateUser("11", "Stan Ciprian", "cipri", "stan01", "false");
        }catch(JDOMException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
   
        
      
    }
    
    
}
