package view;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author vaio
 */
public class BillCalculator {    
    private double finalBill = 0.0;
    private double finalCalculateBillTotal = 0.0;
    private double calculateBillTax = 0.0;
    private double roundedTax = 0.0;
    private double billTotalPlusTax = 0.0;     
    
    
    public BillCalculator() {
        
    }
            
    
    public BillCalculator(List<String> list , String billFormat){
         String formatForBill = list.toString()
        .replace(",", "")  
        .replace("[", "")  
        .replace("]", "") 
        .trim();         
        
        
        List<Double> bill = new ArrayList<>();
        String billItems = formatForBill;
        
        
        String expression = "(?!=\\d\\.\\d\\.)([\\d.]+)";
        Matcher matcher = Pattern.compile(expression).matcher(billItems);
        while (matcher.find()) {
            String doubleAsString = matcher.group();
            finalBill = Double.valueOf(doubleAsString);
            bill.add(finalBill); 
        }
        
        double calculateBill = 0.0;
        for(int i = 0; i < bill.size(); i++){
            calculateBill = calculateBill + bill.get(i);         
        }
        
        finalCalculateBillTotal = (double)(Math.round(calculateBill * 100))/ 100.00;
        calculateBillTax = (calculateBill * 1.055) - calculateBill;
        roundedTax = (double)(Math.round(calculateBillTax * 100))/ 100.00;
        billTotalPlusTax = (double)(Math.round((finalCalculateBillTotal + roundedTax) * 100)) / 100.00;
        
    }


    public double getFinalBill() {
        return finalBill;
    }
   
    public void setFinalBill(double finalBill){
            this.finalBill = finalBill;
    }
    
    
    public double getFinalCalculateBillTotal() {
        return finalCalculateBillTotal;
    }
    
    public void setFinalCalculateBillTotal(double finalCalculateBillTotal){
       this.finalCalculateBillTotal = finalCalculateBillTotal;
    }
    
    
    public double getCalculateBillTax() {
        return calculateBillTax;
    }
    
    public void setCalculateBillTax(double calculateBillTax){
         this.calculateBillTax = calculateBillTax;
    }
   
    
    public double getRoundedTax() {
        return roundedTax;
    }
    
    public void setRoundedTax(double roundedTax){
        this.roundedTax = roundedTax;
    }
    
    
    public double getBillTotalPlusTax() {
        return billTotalPlusTax;
    }
    //
    public void setBillTotalPlusTax(double billTotalPlusTax){
        this.billTotalPlusTax = billTotalPlusTax;
    }

    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.finalBill) ^ (Double.doubleToLongBits(this.finalBill) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.finalCalculateBillTotal) ^ (Double.doubleToLongBits(this.finalCalculateBillTotal) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.calculateBillTax) ^ (Double.doubleToLongBits(this.calculateBillTax) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.roundedTax) ^ (Double.doubleToLongBits(this.roundedTax) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.billTotalPlusTax) ^ (Double.doubleToLongBits(this.billTotalPlusTax) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BillCalculator other = (BillCalculator) obj;
        if (Double.doubleToLongBits(this.finalBill) != Double.doubleToLongBits(other.finalBill)) {
            return false;
        }
        if (Double.doubleToLongBits(this.finalCalculateBillTotal) != Double.doubleToLongBits(other.finalCalculateBillTotal)) {
            return false;
        }
        if (Double.doubleToLongBits(this.calculateBillTax) != Double.doubleToLongBits(other.calculateBillTax)) {
            return false;
        }
        if (Double.doubleToLongBits(this.roundedTax) != Double.doubleToLongBits(other.roundedTax)) {
            return false;
        }
        if (Double.doubleToLongBits(this.billTotalPlusTax) != Double.doubleToLongBits(other.billTotalPlusTax)) {
            return false;
        }
        return true;
    }
    //-----toString method changed ----//
    @Override
    public String toString() {
        return "BillCalculator{" + "finalBill=" + finalBill + ", finalCalculateBillTotal=" + finalCalculateBillTotal + ", calculateBillTax=" + calculateBillTax + ", roundedTax=" + roundedTax + ", billTotalPlusTax=" + billTotalPlusTax + '}';
    }
}


