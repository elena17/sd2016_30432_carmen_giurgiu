package report;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import model.Book;
import org.xml.sax.SAXException;
import view.BookXMLReader;

/**
 *
 * @author vaio
 */
public class CSVReport implements IReport {
    @Override
    public void generateReport(){   
        
        BookXMLReader br = new BookXMLReader();
        ArrayList<Book> books = null;
        
        try{
            books = br.getBooksOutOfStock();
         
        }catch(ParserConfigurationException e){
            e.printStackTrace();
        }catch(SAXException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        
        String booksList = null;
        
        for (int i=0; i<books.size(); i++){
            booksList = "" + books.toString();
        }
        
        try {
                FileWriter fstream = new FileWriter("C:\\Users\\vaio\\Desktop\\SD2016_30432_Carmen_Giurgiu\\BookStore\\books.csv", false);
                BufferedWriter out = new BufferedWriter(fstream);
                out.write(booksList);
                out.close();                
            } catch (Exception e) {
                e.printStackTrace();
            }
        
    }
    
    public static void main(String [] args){
        CSVReport csv = new CSVReport();
        
        csv.generateReport();
    }
}
