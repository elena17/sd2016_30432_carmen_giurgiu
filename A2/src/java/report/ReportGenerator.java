package report;

/**
 *
 * @author vaio
 */
public class ReportGenerator {
    public void generateReports(){
        CSVReport csv = new CSVReport();
        PDFReport pdf = new PDFReport();
        csv.generateReport();
        pdf.generateReport();
    }
}
