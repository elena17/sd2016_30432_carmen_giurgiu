package model;


import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Giurgiu Carmen
 */

public class Book {
    private int id;
    private String bookName;
    private String bookAuthor;
    private String bookIsbn;
    private String category;
    private String image;
    private int quantity;
    private double price;
    
    public Book(){      
    }
    
    public Book(int id, String bookName, String bookAuthor, String bookIsbn, String category, String image,int quantity, double price){
       this.id = id;
       this.bookName = bookName;
       this.bookAuthor = bookAuthor;
       this.bookIsbn = bookIsbn;
       this.category = category;
       this.image = image;
       this.quantity = quantity;
       this.price = price;       
    }
    
    
    public int getId(){
        return id;
    }
    

    public void setId(int id){
        this.id = id;
    }
    
        
    public String getBookName(){
        return bookName;
    }
    

    public void setBookName(String bookName) {        
        this.bookName = bookName;
    }
    
    
    public String getBookAuthor(){
        return bookAuthor;
    }
    
 
    public void setBookAuthor(String bookAuthor){
        this.bookAuthor = bookAuthor;
    }
    
    
    public String getBookIsbn(){
        return bookIsbn;
    }
    

    public void setBookIsbn(String bookIsbn){
        this.bookIsbn = bookIsbn;
    }
    
    
    public String getCategory(){
        return category;
    }
    

    public void setCategory(String category){
        this.category = category;
    }
    
    
    public String getImage(){
        return image;
    }
    

    public void setImage(String image){
        this.image = image;
    }
    
    
    public int getQuantity(){
        return quantity;
    }
    

    public void setQuantity(int quantity){
        this.quantity = quantity;
    }
    
    
    public double getPrice(){
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }
    
    
    @Override
    public int hashCode(){
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Book other = (Book) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Book{" + "id = " + id + ", bookName = " + bookName + ", bookAuthor = " + bookAuthor + ", bookIsbn = " + bookIsbn + ", category = " + category + ", image = " + image + ", quantity = " + quantity + ", price = " + price + '}';
    }
    
}