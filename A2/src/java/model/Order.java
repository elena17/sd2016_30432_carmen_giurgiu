package model;

import java.util.Objects;

/**
 *
 * @author vaio
 */
public class Order {
    
    private int id;
    private String customerName;
    private String orderNumber;
    private String orderDate;
    private double orderTotal;
    private String status;
    
    
     
    public Order(){
        
    }
 
    public Order(int id, String customerName, String orderNumber, String orderDate, double orderTotal, String status) {
        this.id = id;
        this.customerName = customerName;
        this.orderNumber = orderNumber;
        this.orderDate = orderDate;
        this.orderTotal = orderTotal;
        this.status = status;
        
    }

    
    public int getId() {
        return id;
    }

    
    public void setId(int id){
        this.id = id;
    }

     
    public String getCustamerName() {
        return customerName;    }

   
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    
    
    
    public String getOrderNumber() {
        return orderNumber;
    }
   
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

     
    public String getOrderDate() {
        return orderDate;
    }
     
    public void setOrderDate(String orderDate){
        this.orderDate = orderDate;
    }

    
    public double getOrderTotal() {
        return orderTotal;    }

    
    public void setOrderTotal(double orderTotal) {
        this.orderTotal = orderTotal;
    }
    
    
    public String getStatus() {
        return status;
    }

   
    public void setStatus(String status) {
        this.status = status;
    }

     
 

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Order other = (Order) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() {
        return "Order{" + "id = " + id + ", customerName = " + customerName + ", orderNumber = " + orderNumber + ", orderDate = " + orderDate + ", orderTotal = " + orderTotal + ", status = " + status +'}';
    }
    
    
    
    
}
