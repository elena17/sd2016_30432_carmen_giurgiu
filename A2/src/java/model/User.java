package model;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Giurgiu Carmen
 */


public class User {
    private int id;
    private String name;
    private String username;
    private String password;
    private boolean isAdmin;
    
    
    public User(){        
    }
    
    
    public User(int id, String name, String username, String password, boolean isAdmin){
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.isAdmin = isAdmin;                 
    }
    
    public int getId(){
        return id;
    }
    
   
    public void setId(int id){
        this.id = id;
    }
    
    
    public String getName(){
        return name;
    }
    

    public void setName(String name){
        this.name = name;
    }
    
    
    
     public String getUsername(){
        return username;
    }
    
    
    public void setUsername(String username){
        this.username = username;
    }
    
    
    
     public String getPassword(){
        return password;
    }
    
 
    public void setPassword(String password){
        this.password = password;
    }
    
    
    
     public boolean getIsAdmin(){
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin){
        this.isAdmin = isAdmin;
    }
    
    
    @Override
    public int hashCode(){
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "User{" + "id = " + id + ", name = " + name + ", username = " + username + ", password = " + password + ", isAdmin = " + isAdmin + '}';
    }
    
    
}
