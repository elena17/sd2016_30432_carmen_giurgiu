/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Random;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;
import view.OrderXMLWritter;

/**
 *
 * @author vaio
 */
@WebServlet(name = "OrderController", urlPatterns = {"/OrderController"})
public class OrderController extends HttpServlet {
    private final String ORDER_SUBMITTED = "order_submitted.jsp";
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String name = request.getParameter("name");        
        String total = request.getParameter("billedAmount");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date date = new Date();        
        String orderDate = dateFormat.format(date);        
            Random orderNum = new Random();
            int orderNr = orderNum.nextInt(9999999);
        String orderNumber = Integer.toString(orderNr);
        RequestDispatcher view = request.getRequestDispatcher(ORDER_SUBMITTED);
        HttpSession session = request.getSession();
        
        OrderXMLWritter or = new OrderXMLWritter();
        
        if (name != null){
          try{
               or.addOrder("11", name, orderDate, orderNumber,  total, "pending");
            }catch(IOException e){
                e.printStackTrace();
            }catch (ParserConfigurationException e){
                e.printStackTrace();
            }catch(TransformerConfigurationException e){
                e.printStackTrace();;
            }catch(TransformerException e){
                e.printStackTrace();
            }catch(SAXException e){
                e.printStackTrace();
            }
                view.forward(request, response);
                } else {
                    String errorMessage = " All Fields Must Be Filled Out In Order To Create A New Record";
                    session.setAttribute("errorMessage", errorMessage);                    
                    response.sendRedirect("error_page_order.jsp");
                }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
