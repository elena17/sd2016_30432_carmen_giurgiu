/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import model.Order;
import org.jdom2.JDOMException;
import org.xml.sax.SAXException;
import view.OrderXMLDelete;
import view.OrderXMLReader;
import view.OrderXMLUpdate;
import view.OrderXMLWritter;

/**
 *
 * @author vaio
 */
@WebServlet(name = "OrderUserController", urlPatterns = {"/OrderUserController"})
public class OrderUserController extends HttpServlet {

    private final String ADMIN_RECORDS_PAGE = "order_records.jsp";
    private final String RECORDS_UPDATE_RE_DIRECT = "update_order.jsp";
    private final String SERVLET_ACTION = "action";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        String action = request.getParameter(SERVLET_ACTION);
        String id = request.getParameter("id");
        String customerName = request.getParameter("customerName");
        String orderNumber = request.getParameter("orderNumber");
        String orderDate = request.getParameter("orderDate");
        String total = request.getParameter("total");
        String status = request.getParameter("status");   
        RequestDispatcher view = request.getRequestDispatcher(ADMIN_RECORDS_PAGE);
        RequestDispatcher redirect = request.getRequestDispatcher(RECORDS_UPDATE_RE_DIRECT);
        RequestDispatcher errorPage = request.getRequestDispatcher(getServletContext().getInitParameter("error_page_500.jsp"));
        
        OrderXMLDelete od = new OrderXMLDelete();
        OrderXMLReader or = new OrderXMLReader();
        OrderXMLUpdate ou = new OrderXMLUpdate();
        OrderXMLWritter ow = new OrderXMLWritter();
        
        
        
        //--------------BASIC SERVER C.R.U.D PROCESSES FOR ORDERS---------------------// 
        
        
        switch (action){
            case "deleteRecord":
                if (id != null){
                    try {
                        int idOrder = Integer.parseInt(id);                      
                        
                        if (or.getOrderById(idOrder) != null){
                            od.deleteOrder(id);
                        }else {
                            String errorMessage = "Order not found!";
                            request.setAttribute("errorMessage", errorMessage);
                            errorPage.forward(request, response);
                        }
                    
                    }catch(ParserConfigurationException e){
                        e.printStackTrace();
                    }catch(SAXException e){
                        e.printStackTrace();
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                     
                    
                    view.forward(request, response);
                }else {
                    String errorMessage = "Must Give An ID To Delete A Record";
                    request.setAttribute("errorMessage", errorMessage);
                    errorPage.forward(request, response);
                }
            break;
                
                
            case "createNewRecord":
                if(id != null && customerName != null && orderNumber != null && orderDate != null && total != null && status != null){
                  
                
                try{
                  ow.addOrder(id, customerName, orderNumber, orderDate, total, status);
                }catch(IOException e){
                    e.printStackTrace();
                }catch (ParserConfigurationException e){
                    e.printStackTrace();
                }catch(TransformerConfigurationException e){
                    e.printStackTrace();;
                }catch(TransformerException e){
                    e.printStackTrace();
                }catch(SAXException e){
                    e.printStackTrace();
                }
                
                
                view.forward(request, response);
                } else {
                    String errorMessage = " All Fields Must Be Filled Out In Order To Create A New Record";
                    request.setAttribute("errorMessage", errorMessage);
                    errorPage.forward(request, response);
                }
            break;
            
            case "findRecordToUpdate":
                if(id != null){
                int idOrder = Integer.parseInt(id);
                Order findRecord = null;
                try{
                    findRecord = or.getOrderById(idOrder);
                }catch(ParserConfigurationException e){
                    e.printStackTrace();
                }catch(SAXException e){
                    e.printStackTrace();
                }catch(IOException e){
                    e.printStackTrace(); 
                }              
              
                request.setAttribute("id", findRecord.getId());
                request.setAttribute("customerName", findRecord.getCustamerName());
                request.setAttribute("orderNumber", findRecord.getOrderNumber());
                request.setAttribute("orderDate", findRecord.getOrderDate());
                request.setAttribute("total", findRecord.getOrderTotal());
                request.setAttribute("status", findRecord.getStatus());
                
                
                redirect.forward(request, response);
              } else {
                  String errorMessage = "Must Enter An ID To Find Record";
                  request.setAttribute("errorMessage", errorMessage);
                  errorPage.forward(request, response);
              }
            break;
                
                case "updateRecord" :
                if(id != null && status != null){
                    
                try{
                    ou.updateOrder(id, null, null, null, null, status);
                }catch(JDOMException e){
                    e.printStackTrace();
                }catch(IOException e){
                    e.printStackTrace();
                }
                
                view.forward(request, response); 
                } else {
                  String errorMessage = "Must Have All Fields Completed";
                  request.setAttribute("errorMessage", errorMessage);
                  errorPage.forward(request, response);
                }
                break;
            
            
     }      
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
