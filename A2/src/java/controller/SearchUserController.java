/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import model.Book;
import org.xml.sax.SAXException;
import view.BookXMLReader;

/**
 *
 * @author vaio
 */
public class SearchUserController extends HttpServlet {

    private final String USER_SEARCH_BY_CATEGORY = "search_by_category.jsp";
    private final String USER_SEARCH_BY_TITLE = "search_by_title.jsp";
    private final String USER_SEARCH_BY_AUTHOR = "search_by_author.jsp";
    private final String SERVLET_ACTION = "action";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String action = request.getParameter(SERVLET_ACTION); 
        String category = request.getParameter("category");
        String bookName = request.getParameter("bookName");
        String bookAuthor = request.getParameter("bookAuthor");       
        
        
        RequestDispatcher viewCategory = request.getRequestDispatcher(USER_SEARCH_BY_CATEGORY);
        RequestDispatcher viewTitle = request.getRequestDispatcher(USER_SEARCH_BY_TITLE);
        RequestDispatcher viewAuthor = request.getRequestDispatcher(USER_SEARCH_BY_AUTHOR);
                
        HttpSession session = request.getSession();
        
        BookXMLReader br = new BookXMLReader();
        
        switch (action){
            case "searchByCategory":
                if (category != null){
                    List<Book> books = null;
                    try{
                        books = br.getBookByCategory(category);
                        String sendBooks = null;
                        for (int i=0; i<books.size(); i++){
                            int id = books.get(i).getId();
                            String name = books.get(i).getBookName();
                            String author = books.get(i).getBookAuthor();
                            String isbn = books.get(i).getBookIsbn();
                            String categ = books.get(i).getCategory();
                            String img = books.get(i).getImage();
                            int quantity = books.get(i).getQuantity();
                            double price = books.get(i).getPrice();  
                            
                            sendBooks += "" + id + ", " + name + ", " +author + ", " + isbn + "," + categ + ", " + img + "," + quantity + "," + price; 
                        
                        }
                        
                        
                        if(books != null){
                            //response.sendRedirect("search_by_category.jsp?bookList=" + sendBooks);
                            request.setAttribute("booksList", sendBooks);
                            viewCategory.forward(request, response);
                        }else{
                            
                            String errorMessage = "No books for this category!";
                            session.setAttribute("errorMessage", errorMessage);                            
                            response.sendRedirect("error_page_search.jsp");
                       
                        }
                        
                    }catch(ParserConfigurationException e){
                        e.printStackTrace();
                    }catch(SAXException e){
                        e.printStackTrace();
                    }catch(IOException e){
                        e.printStackTrace();
                    }             
                   
                }else {
                    String errorMessage = "Must Give A Category To Search For";
                    session.setAttribute("errorMessage", errorMessage);                    
                    response.sendRedirect("error_page_search.jsp");
                }                 
            break;   
                
                
                
            case "searchByTitle":
                if (bookName != null){
                    List<Book> books = null;
                    try{
                        books = br.getBookByTitle(bookName);
                        String sendBooks = null;
                        for (int i=0; i<books.size(); i++){
                            int id = books.get(i).getId();
                            String name = books.get(i).getBookName();
                            String author = books.get(i).getBookAuthor();
                            String isbn = books.get(i).getBookIsbn();
                            String categ = books.get(i).getCategory();
                            String img = books.get(i).getImage();
                            int quantity = books.get(i).getQuantity();
                            double price = books.get(i).getPrice();  
                            
                            sendBooks += "" + id + ", " + name + ", " +author + ", " + isbn + "," + categ + ", " + img + "," + quantity + "," + price; 
                        
                        }
                        
                        
                        if(books != null){
                            //response.sendRedirect("search_by_category.jsp?bookList=" + sendBooks);
                            request.setAttribute("booksList", sendBooks);
                            viewTitle.forward(request, response);
                        }else{
                            
                            String errorMessage = "No books for with this title!";
                            session.setAttribute("errorMessage", errorMessage);                            
                            response.sendRedirect("error_page_search.jsp");
                       
                        }
                        
                    }catch(ParserConfigurationException e){
                        e.printStackTrace();
                    }catch(SAXException e){
                        e.printStackTrace();
                    }catch(IOException e){
                        e.printStackTrace();
                    }             
                   
                }else {
                    String errorMessage = "Must Give A Title To Search For";
                    session.setAttribute("errorMessage", errorMessage);                    
                    response.sendRedirect("error_page_search.jsp");
                }                 
            break;               
        
        
             case "searchByAuthor":
                if (bookAuthor != null){
                    List<Book> books = null;
                    try{
                        books = br.getBookByAuthor(bookAuthor);
                        String sendBooks = null;
                        for (int i=0; i<books.size(); i++){
                            int id = books.get(i).getId();
                            String name = books.get(i).getBookName();
                            String author = books.get(i).getBookAuthor();
                            String isbn = books.get(i).getBookIsbn();
                            String categ = books.get(i).getCategory();
                            String img = books.get(i).getImage();
                            int quantity = books.get(i).getQuantity();
                            double price = books.get(i).getPrice();  
                            
                            sendBooks += "" + id + ", " + name + ", " +author + ", " + isbn + "," + categ + ", " + img + "," + quantity + "," + price; 
                        
                        }
                        
                        
                        if(books != null){
                            //response.sendRedirect("search_by_category.jsp?bookList=" + sendBooks);
                            request.setAttribute("booksList", sendBooks);
                            viewTitle.forward(request, response);
                        }else{
                            
                            String errorMessage = "No books for with this title!";
                            session.setAttribute("errorMessage", errorMessage);                            
                            response.sendRedirect("error_page_search.jsp");
                       
                        }
                        
                    }catch(ParserConfigurationException e){
                        e.printStackTrace();
                    }catch(SAXException e){
                        e.printStackTrace();
                    }catch(IOException e){
                        e.printStackTrace();
                    }             
                   
                }else {
                    String errorMessage = "Must Give A Title To Search For";
                    session.setAttribute("errorMessage", errorMessage);                    
                    response.sendRedirect("error_page_search.jsp");
                }                 
            break;               
        
                
                
         }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

    
    
    