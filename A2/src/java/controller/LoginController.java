/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import view.UserXMLReader;
import model.User;
import org.xml.sax.SAXException;

/**
 *
 * @author vaio
 */
@WebServlet(name = "LoginController", urlPatterns = {"/LoginController"})
public class LoginController extends HttpServlet {
    
    private final String ADMIN_LOGIN = "admin_panel.jsp";
    private final String USER_LOGIN = "user_panel.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        User user = null;
        
        String username = request.getParameter("us");
        String password = request.getParameter("ps");        
        RequestDispatcher adminView = request.getRequestDispatcher(ADMIN_LOGIN);      
        RequestDispatcher userView = request.getRequestDispatcher(USER_LOGIN);
        HttpSession session = request.getSession();
        
        
        UserXMLReader uw = new UserXMLReader();        
        try{            
        
            user = uw.getUser(username, password);  
            
            if (user != null){
                    if (user != null && user.getIsAdmin()){
                        adminView.forward(request, response);  
                    }
                    else{
                        if (user != null && !user.getIsAdmin())
                        userView.forward(request, response);
                    }
            }  else{
                    String errorMessage = "User not found!";
                    session.setAttribute("errorMessage", errorMessage);                            
                    response.sendRedirect("error_page_user.jsp");
            }
               
        }catch(ParserConfigurationException e){
            e.printStackTrace();
        }catch(SAXException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        
                    
        
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    
    

}
