/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import view.BillCalculator;

/**
 *
 * @author vaio
 */
@WebServlet(name = "MainController", urlPatterns = {"/MainController"})
public class MainController extends HttpServlet {

    private final String SHOPPING_CART_PAGE = "shoping_cart.jsp";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        response.setContentType("text/html");
        //String SHOPPING_CART_PAGE = getServletContext().getInitParameter("shoppingCartPage");
        HttpSession session = request.getSession();
        String[] bookOrders = request.getParameterValues("itemSelected");
        
        
        //-------------------HANDLES THE PROCESS OF GETTING BOOKS, GETTING BILL, RE-DIRECTS TO CHECKOUT --------------//
        if (bookOrders != null) {
            List<String> itemsSelected = new ArrayList();
            itemsSelected.add(Arrays.toString(bookOrders));
            String formatString = itemsSelected.toString()
                    .replace(",", "<br />")
                    .replace("[", "<p>")
                    .replace("]", "</p>")
                    .replace("null", "You have no items in your shopping cart")
                    .trim();

//            for (Iterator<String> it = itemsSelected.iterator(); it.hasNext();) {
//                String element = it.next();
//                if ("null".equals(element)) {
//                    it.remove();
//                }
//            }

            BillCalculator billCalculator = new BillCalculator(itemsSelected, formatString);
            session.setAttribute("orderedItems", formatString);
            session.setAttribute("billTotal", billCalculator.getFinalCalculateBillTotal());
            session.setAttribute("billTax", billCalculator.getRoundedTax());
            session.setAttribute("billTotalPlusTax", billCalculator.getBillTotalPlusTax());
            response.sendRedirect(SHOPPING_CART_PAGE);
        } else {
            String errorMessage = "You Must Make An Order Selection Before Processing Checkout";
            session.setAttribute("errorMessage", errorMessage);
            response.sendRedirect("error_page_order.jsp");
        }
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
