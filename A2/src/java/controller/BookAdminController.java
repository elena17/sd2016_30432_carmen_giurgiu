package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import model.Book;
import org.jdom2.JDOMException;
import org.xml.sax.SAXException;
import view.BookXMLDelete;
import view.BookXMLReader;
import view.BookXMLUpdate;
import view.BookXMLWritter;

/**
 *
 * @author vaio
 */
@WebServlet(name = "BookAdminController", urlPatterns = {"/BookAdminController"})
public class BookAdminController extends HttpServlet {
    
    
    private final String ADMIN_RECORDS_PAGE = "book_records.jsp";
    private final String RECORDS_UPDATE_RE_DIRECT = "update_book.jsp";
    private final String SERVLET_ACTION = "action";
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        String action = request.getParameter(SERVLET_ACTION);
        String id = request.getParameter("id");
        String bookName = request.getParameter("bookName");
        String bookAuthor = request.getParameter("bookAuthor");
        String bookIsbn = request.getParameter("bookIsbn");
        String category = request.getParameter("category");
        String image = request.getParameter("image");
        String quantity = request.getParameter("quantity");
        String price = request.getParameter("price");
        RequestDispatcher view = request.getRequestDispatcher(ADMIN_RECORDS_PAGE);
        RequestDispatcher redirect = request.getRequestDispatcher(RECORDS_UPDATE_RE_DIRECT);        
        HttpSession session = request.getSession();
        
        BookXMLDelete bd = new BookXMLDelete();
        BookXMLReader br = new BookXMLReader();
        BookXMLUpdate bu = new BookXMLUpdate();
        BookXMLWritter bw = new BookXMLWritter();
        
        
        
        //--------------BASIC SERVER C.R.U.D PROCESSES FOR BOOKS---------------------// 
        
        
        switch (action){
            case "deleteRecord":
                if (id != null){
                    try {
                        int idBook = Integer.parseInt(id);                      
                        
                        if (br.getBookById(idBook) != null){
                            bd.deleteBook(id);
                            view.forward(request, response);
                        }else {
                            String errorMessage = "Book not found!";
                            session.setAttribute("errorMessage", errorMessage);                            
                            response.sendRedirect("error_page_book.jsp");
                        }
                    
                    }catch(ParserConfigurationException e){
                        e.printStackTrace();
                    }catch(SAXException e){
                        e.printStackTrace();
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                     
                    
                   
                }else {
                    String errorMessage = "Must Give An ID To Delete A Record";
                    session.setAttribute("errorMessage", errorMessage);                    
                    response.sendRedirect("error_page_book.jsp");
                }
                 
            break;
                
                
                
            case "createNewRecord":
                if(bookName != null && bookAuthor != null && bookIsbn != null && category != null && image != null && quantity != null && price != null){
                
                
                
                try{
                  bw.addBook(id, bookName, bookAuthor, bookIsbn, category, image, quantity, price);
                }catch(IOException e){
                    e.printStackTrace();
                }catch (ParserConfigurationException e){
                    e.printStackTrace();
                }catch(TransformerConfigurationException e){
                    e.printStackTrace();;
                }catch(TransformerException e){
                    e.printStackTrace();
                }catch(SAXException e){
                    e.printStackTrace();
                }
                
                
                view.forward(request, response);
                } else {
                    String errorMessage = " All Fields Must Be Filled Out In Order To Create A New Record";
                    session.setAttribute("errorMessage", errorMessage);                    
                    response.sendRedirect("error_page_book.jsp");
                }
            break;
            
            case "findRecordToUpdate":
                if(id != null){
                int idBook = Integer.parseInt(id);
                Book findRecord = null;
                try{
                    findRecord = br.getBookById(idBook);  
                    if (findRecord == null){
                        String errorMessage = "Record Not Found!";
                        session.setAttribute("errorMessage", errorMessage);                    
                        response.sendRedirect("error_page_book.jsp");
                    }
                    
                }catch(ParserConfigurationException e){
                    e.printStackTrace();
                }catch(SAXException e){
                    e.printStackTrace();
                }catch(IOException e){
                    e.printStackTrace(); 
                }              
              
                request.setAttribute("id", findRecord.getId());
                request.setAttribute("bookName", findRecord.getBookName());
                request.setAttribute("bookAuthor", findRecord.getBookAuthor());
                request.setAttribute("bookIsbn", findRecord.getBookIsbn());
                request.setAttribute("category", findRecord.getCategory());
                request.setAttribute("image", findRecord.getImage());
                request.setAttribute("quantity", findRecord.getQuantity());
                request.setAttribute("price", findRecord.getPrice());
                
                redirect.forward(request, response);
              } else {
                  String errorMessage = "Must Enter An ID To Find Record";
                  session.setAttribute("errorMessage", errorMessage);                    
                  response.sendRedirect("error_page_book.jsp");
              }
            break;
                
           case "updateRecord" :
                if(id != null && bookName != null && bookAuthor != null && bookIsbn != null && category != null && image != null && quantity != null && price != null){
                    
                try{
                    bu.updateBook(id, bookName, bookAuthor, bookIsbn, category, image, quantity, price);
                }catch(JDOMException e){
                    e.printStackTrace();
                }catch(IOException e){
                    e.printStackTrace();
                }
                
                view.forward(request, response); 
                } else {
                    String errorMessage = "Must Have All Fields Completed";
                    session.setAttribute("errorMessage", errorMessage);                    
                    response.sendRedirect("error_page_book.jsp");
                }
                break;
            
            
     }
        
        
 }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
