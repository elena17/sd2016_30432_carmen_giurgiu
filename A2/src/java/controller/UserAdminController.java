package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import model.User;
import org.jdom2.JDOMException;
import org.xml.sax.SAXException;
import view.UserXMLDelete;
import view.UserXMLReader;
import view.UserXMLUpdate;
import view.UserXMLWritter;

/**
 *
 * @author vaio
 */
public class UserAdminController extends HttpServlet {

    private final String ADMIN_RECORDS_PAGE = "user_records.jsp";
    private final String RECORDS_UPDATE_RE_DIRECT = "update_user.jsp";
    private final String SERVLET_ACTION = "action";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String action = request.getParameter(SERVLET_ACTION);
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String isAdmin = request.getParameter("isAdmin");        
        RequestDispatcher view = request.getRequestDispatcher(ADMIN_RECORDS_PAGE);
        RequestDispatcher redirect = request.getRequestDispatcher(RECORDS_UPDATE_RE_DIRECT);
        RequestDispatcher errorPage = request.getRequestDispatcher(getServletContext().getInitParameter("error_page_500.jsp"));
        HttpSession session = request.getSession();
        
        UserXMLDelete ud = new UserXMLDelete();
        UserXMLReader ur = new UserXMLReader();
        UserXMLUpdate uu = new UserXMLUpdate();
        UserXMLWritter uw = new UserXMLWritter();
        
        switch (action){
            case "deleteRecord":
                if (id != null){
                    try {
                        int idUser = Integer.parseInt(id);                      
                        
                        if (ur.getUserById(idUser) != null){
                            ud.deleteUser(id);
                            view.forward(request, response);
                        }else {
                            String errorMessage = "User not found!";
                            session.setAttribute("errorMessage", errorMessage);                            
                            response.sendRedirect("error_page_user_admin.jsp");
                        }
                    
                    }catch(ParserConfigurationException e){
                        e.printStackTrace();
                    }catch(SAXException e){
                        e.printStackTrace();
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                     
                    
                }else {
                    String errorMessage = "Must Give An ID To Delete A Record";
                    session.setAttribute("errorMessage", errorMessage);                            
                    response.sendRedirect("error_page_user_admin.jsp");
                }
            break;
                
            case "createNewRecord":
                if(id != null && name != null && username != null && password != null && isAdmin != null){
                
                
                
                try{
                  uw.addUser(id, name, username, password, isAdmin);
                }catch(IOException e){
                    e.printStackTrace();
                }catch (ParserConfigurationException e){
                    e.printStackTrace();
                }catch(TransformerConfigurationException e){
                    e.printStackTrace();;
                }catch(TransformerException e){
                    e.printStackTrace();
                }catch(SAXException e){
                    e.printStackTrace();
                }
                
                
                view.forward(request, response);
                } else {
                    String errorMessage = " All Fields Must Be Filled Out In Order To Create A New Record";
                    session.setAttribute("errorMessage", errorMessage);                            
                    response.sendRedirect("error_page_user_admin.jsp");
                }
            break;
            
            case "findRecordToUpdate":
                if(id != null){
                int idUser = Integer.parseInt(id);
                User findRecord = null;
                try{
                    findRecord = ur.getUserById(idUser);                    
                }catch(ParserConfigurationException e){
                    e.printStackTrace();
                }catch(SAXException e){
                    e.printStackTrace();
                }catch(IOException e){
                    e.printStackTrace(); 
                }              
              
                request.setAttribute("id", findRecord.getId());
                request.setAttribute("name", findRecord.getName());
                request.setAttribute("username", findRecord.getUsername());
                request.setAttribute("password", findRecord.getPassword());
                request.setAttribute("isAdmin", findRecord.getIsAdmin());
                
                
                redirect.forward(request, response);
              } else {
                  String errorMessage = "Must Enter An ID To Find Record";
                  session.setAttribute("errorMessage", errorMessage);                            
                    response.sendRedirect("error_page_user_admin.jsp");
              }
            break;
                
                case "updateRecord" :
                if(id != null && name != null && username != null && password != null && isAdmin != null){
                    
                try{
                    uu.updateUser(id, name, username, password, isAdmin);
                }catch(JDOMException e){
                    e.printStackTrace();
                }catch(IOException e){
                    e.printStackTrace();
                }
                
                view.forward(request, response); 
                } else {
                  String errorMessage = "Must Have All Fields Completed";
                  session.setAttribute("errorMessage", errorMessage);                            
                    response.sendRedirect("error_page_user_admin.jsp");
                }
                break;
            
        
        
        }
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
