<%-- 
    Document   : reports
    Created on : 09.05.2016, 01:39:54
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="admin_panel.jsp">Admin</a></li>
            </ul>
            
            
            <div id="gradient-separator"></div>
            <div id="content">
                <h1>Generate Reports</h1>
                <hr>
                </br>
                <form id="generateReport" name="generateReport" method="POST" action="ReportAdminController">              
                    <input id="submitReport" type="submit" value="Generate Reports">
                    <a href="admin_panel.jsp"><button id="buttonReturn" type="button">Return To Admin Panel</button></a>       
                </form>            
            </div>>
                 
            
              
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>
