<%-- 
    Document   : update_order
    Created on : 11.05.2016, 02:16:42
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.4.2.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script src="js/orderFormsValidation.js" type="text/javascript"></script>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="user_panel.jsp">User</a></li>
            </ul>
            
            
            
            <div id="gradient-separator"></div>
         <div id="content">
            <h1>Please Fill Out The Required Fields Below And Submit To Create A New Record</h1>
            <hr>
            <span>Search for a record you wish to update</span>
            <p></p>
            
             <form id="newOrderForm" name="newOrderForm" method="POST" action="OrderUserController?action=findRecordToUpdate">  
                  <p>ID: <input id="id" name="id" value="${id}"> <input id="findRecordToUpdate" name="submit" type="submit" value="Search For Record"></p>
             </form>
            <p></p>
            <hr>
            
            <form id="newOrderForm" name="OrderForm" method="POST" action="OrderUserController?action=updateRecord">  
                <p> Id: <input id="id" name="id" value="${id}"></p>
                <p> Customer Name: <input id="customerName" name="customerName" value="${customerName}"></p>
                <p> Order Number: <input id="orderNumber" name="orderNumber" value="${orderNumber}"></p>
                <p> Order Date: <input id="orderDate" name="orderDate" value="${orderDate}"></p>
                <p> Total: <input id="total" name="total" value="${total}"></p>
                <p> Status: <input id="status" name="status" value="${status}"></p>                  
                <p> <input id="submitUpdate" name="submit" type="submit" value="Update Record">
            </form>
        
            <div id="buttonSpace" Style="padding-bottom: 25px;">
                <a href="user_records.jsp"><button id="buttonReturn" type="button">Return To User Records</button></a>
            </div>
        </div>     
            
            
            
            
            </br>
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>
