<%-- 
    Document   : error_page_book
    Created on : 11.05.2016, 17:03:44
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="admin_panel.jsp">Admin</a></li>
            </ul>
            
           
             <div id="gradient-separator"></div>
         <div id="content">
             <h1>ERROR!</h1>
             <hr>
             <p>&nbsp;</p>
             <ul>
                 <li>
                 ${errorMessage}
                 </li>
                 <li>
                 <a href="book_records.jsp">Return To Book Records</a>  
                 </li>
             </ul>
             
         </div>
         <div id="gradient-separator"></div>
            
            
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>
