<%-- 
    Document   : search_by_title
    Created on : 11.05.2016, 22:36:22
    Author     : vaio
--%>

<%@page import="java.io.IOException"%>
<%@page import="org.xml.sax.SAXException"%>
<%@page import="javax.xml.parsers.ParserConfigurationException"%>
<%@page import="model.Book"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.net.URLDecoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Book Store</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="user_panel.jsp">User</a></li>
            </ul>
            
            
            <div id="records-content">        
                    <table style="width: 100%">
                    <tr>
                        <th>ID</th>
                        <th>Book Name</th>
                        <th>Book Author</th>
                        <th>ISBN</th>
                        <th>Category</th>
                        <th>Image</th>
                        <th>Quantity</th>
                        <th>Price</th>                 
                    </tr>
                  
                    <%
                        String id=null, bookName=null,  bookAuthor=null, isbn=null, category=null, image=null, quantity=null, price=null;
                        
                        String booksList = (String)request.getAttribute("booksList");
                        String [] bookItems = booksList.split(",");
                        
                        int i = bookItems.length/8;
                        for (int j=0; j<i; j++ ){
                            id = bookItems[0];
                            bookName = bookItems[1];
                            bookAuthor = bookItems[2];
                            isbn = bookItems[3];
                            category = bookItems[4];
                            image = bookItems[5];
                            quantity = bookItems[6];
                            price = bookItems[7];
                            
                           
                     %>

              
                        <tr>   
                            <td><%= id %></td>
                            <td><%= bookName %></td>
                            <td><%= bookAuthor %></td>
                            <td><%= isbn %></td>
                            <td><%= category %></td>
                            <td><%= image %></td>
                            <td><%= quantity %></td>
                            <td><%= price %></td>
                        </tr>

                    <% 
                    }
                    %>           
                  
              
                
              </table>
                <div id="panelButtons">                
                  <a href="search.jsp"><button id="buttonReturn" type="button">Back</button></a>
                </div>
              </div>
            
      
      
            <div id="gradient-separator"></div>               
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div> 
    </body>
    
</html>