<%-- 
    Document   : login.jsp
    Created on : 04.05.2016, 02:30:28
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Log in</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.4.2.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script src="js/loginFormValidation.js" type="text/javascript"></script>
        
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="login.jsp">Log in</a></li>
            </ul>
            
            <div id="gradient-separator"></div>
            <div id="content">
            <h1>Fill in form to log in!</h1>
            <hr>
            <p></p>
            <p></p>
            <p></p>
            <form  id="loginForm" name="loginForm" method="POST" action="LoginController" >
                        Username: <input id="us" name="us" type="text" >
                        <br />
                        </br>
                        </br>
                        Password: <input id="ps" name="ps" type="password"  />
                        </br>
                        </br>
                        <input id="loginButton" type="submit" value="Submit" />
            </form>
            </div>
           
            <div id="gradient-separator"></div>
               
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
            
        </div>    
    </body>
    
</html>
