<%-- 
    Document   : shoping_cart
    Created on : 11.05.2016, 15:53:54
    Author     : vaio
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Book Store</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
                <header>
                    <img src = "images/dummyBanner.jpg" alt=""/>
                </header>
            
            
                <ul id ="nav">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="book_store.jsp">Store</a></li>
                    <li><a href="login.jsp">Log in</a></li>
                </ul>
            
                
                <div id="gradient-separator"></div>
         <div Style="padding-top: 30px;" id="content">
     
             <hr>
             <h1>Your Order</h1>
             <hr>
                 
        <c:forEach var="item" items="${orderedItems}" varStatus="rowCount">
            ${orderedItems}          
        </c:forEach>

          <hr>
        <p>Total: $ ${billTotal}</p>
        <p>Tax:$ ${billTax}</p>
        <p>Bill Total Plus Tax:$ <span id="bill">${billTotalPlusTax}</span>
         </div>
         <div id="button_center">
          <a href="checkout.jsp"><Button id="payment" type="submit" value="Cancel Order">Continue To Checkout</button></a>
         <a href="book_store.jsp"><Button id="cancelOrder" type="submit" value="Cancel Order">Cancel Order</button></a>
         </div>
         <div id="gradient-separator"></div>
            
            

                <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
                </footer>

            
        </div>    
    </body>
    
</html>
