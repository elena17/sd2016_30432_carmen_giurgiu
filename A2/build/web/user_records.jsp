<%-- 
    Document   : customer_records
    Created on : 09.05.2016, 01:38:47
    Author     : vaio
--%>

<%@page import="java.util.List"%>
<%@page import="model.User"%>
<%@page import="view.UserXMLReader"%>
<% UserXMLReader ur = new UserXMLReader(); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="admin_panel.jsp">Admin</a></li>
            </ul>            
            
            
            <div id="records-content">        
                  <table style="width: 100%">
                  <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Is Administrator</th>                                       
                  </tr>
                  
                  <%
                List<User> users =  ur.getAllNonAdminUsers();
                for(int i = 0; i < users.size(); i++){
                    int id = users.get(i).getId();
                    String name = users.get(i).getName(); 
                    String username = users.get(i).getUsername();
                    String password = users.get(i).getPassword();
                    boolean isAdmin = users.get(i).getIsAdmin();
              %>
              
              
                <tr>   
                      <td><%= id %></td>
                      <td><%= name %></td>
                      <td><%= username %></td>
                      <td><%= password %></td>
                      <td><%= isAdmin %></td>                      <
                
               
                </tr> 
              
              <% 
                 }
              %>
                  
              
                
              </table>
              <div id="panelButtons">
                <a href="delete_user.jsp"><button id="deleteRecode" type="button" value="Delete Record">Delete User</button></a>
                <a href="new_user.jsp"><button id="createRecord" type="button" value="Create New Record">Create Record</button></a>
                <a href="update_user.jsp"><button id="updateRecord" type="button" value="Update Record">Update Record</button></a>
              <a href="admin_panel.jsp"><button id="buttonReturn" type="button">Return To Admin Panel</button></a>
              </div>
              </div>
            
                 
            
               
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>
