/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    
    $('input[type="checkbox"]').click(function(){
    var title = $(this).closest('.module').find('.bookName').html();
    // If the checkbox is checked, add the item to the ul.
    if($(this).attr('checked')){
        var html = '<li title="' + title + '" name="cartItems">' + title + '</li>';
        $('ul.cartItemsSelected').append(html);
        $('#submit').show();
    } else {     
        // if the checkbox is unchecked, remove the item from the ul.
        $('li[title="' + title + '"]').remove();
    }
 });

  $('#cssmenu > ul > li:has(ul)').addClass("has-sub");

  $('#cssmenu > ul > li > a').click(function() {
    var checkElement = $(this).next();
    
    $('#cssmenu li').removeClass('active');
    $(this).closest('li').addClass('active');	
    
    
    if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
      $(this).closest('li').removeClass('active');
      checkElement.slideUp('normal');
    }
    
    if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
      $('#cssmenu ul ul:visible').slideUp('normal');
      checkElement.slideDown('normal');
    }
    
    if (checkElement.is('ul')) {
      return false;
    } else {
      return true;	
    }		
  });
  

});



