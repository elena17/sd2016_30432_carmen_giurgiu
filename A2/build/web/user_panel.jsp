<%-- 
    Document   : user_panel
    Created on : 04.05.2016, 18:37:26
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="user_panel.jsp">User</a></li>
            </ul>
            
            <h1>User panel</h1>
            
            
            <div id="user-content">           
                <a href="search.jsp"><button type="button" id="findRecords" value="Book Records">Search</button></a>
                <a href="order_records.jsp"><button type="button" id="findRecords" value="Customer Records">Order Records</button></a>
                
            </div>
            
            
               
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>


