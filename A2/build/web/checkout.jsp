<%-- 
    Document   : checkout
    Created on : 11.05.2016, 16:06:06
    Author     : vaio
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Book Store</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.4.2.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script src="js/checkoutValidation.js" type="text/javascript"></script>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="login_panel.jsp">Log in</a></li>
            </ul>
            
            
             <div id="gradient-separator"></div>
            <div Style="padding-top: 30px;" id="content">

                <hr>
                <h1>Checkout:</h1>
                <hr>
                  <form id="paymentForm" name="paymentForm" method="POST" action="OrderController">
                        <p>Name:<input id="name" name="name" type="text" maxlength="30"></p>               
                        <p>Credit Card Type:<select>
                                <option value="" disabled="disabled" selected="selected">Select a Card</option>
                                <option value="1">Visa</option>
                                <option value="2">Mastercard</option>
                                <option value="2">Discover</option>
                                <option value="2">American Express</option>
                            </select></p>
                                            
                        
                <p>Total: $ ${billTotal}</p>
                <p>Tax:$ ${billTax}</p>
                <p>Bill Total Plus Tax:$ <input id="bill" name="billedAmount" type="text" readOnly="readonly" value="${billTotalPlusTax}" ></p>
                <div id="buttonSpace" style="padding-bottom: 20px;">
                <input id="submitCheckout" type="submit" value="Checkout">
                </div>
                </form>
            </div>

            <div id="gradient-separator"></div>
            
                 
      
            <div id="gradient-separator"></div>
               
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div> 
    </body>
    
</html>
