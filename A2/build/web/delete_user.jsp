<%-- 
    Document   : delete_user
    Created on : 09.05.2016, 03:51:24
    Author     : vaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.4.2.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script src="js/userFormsValidation.js" type="text/javascript"></script>
    </head>
    
   
    <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="admin_panel.jsp">Admin</a></li>
            </ul>
            
           
            
             <div id="gradient-separator"></div>
         <div id="content">
        <h1>Enter The ID Of The Record You Wish To Delete</h1>
        <hr>
        <span>Remember This Is A Permanent Action</span>
        <p></p>
        
        <form id="deleteUserForm" name="deleteBookForm" method="POST" action="UserAdminController?action=deleteRecord">            
            ID: <input id="id" name="id" value="${id}">
            <input id="submitDelete" name="submit" type="submit" value="Delete Record">
        </form>
        
        <div id="buttonSpace" Style="padding-top: 50px;">
            <a href="user_records.jsp"><button id="buttonReturn" type="button">Return To User Records</button></a>
        </div>
        </div>     
            
             
            
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>