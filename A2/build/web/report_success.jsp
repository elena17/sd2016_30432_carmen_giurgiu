<%-- 
    Document   : report_success
    Created on : 12.05.2016, 14:01:42
    Author     : vaio
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Panel</title>        
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
 <body>
        <div id = "wrapper">
            <header>
                <img src = "images/dummyBanner.jpg" alt=""/>
            </header>
            
            <ul id ="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="book_store.jsp">Store</a></li>
                <li><a href="admin_panel.jsp">Admin</a></li>
            </ul>           
         
            <div id="gradient-separator"></div>
                <div id="content">
                 <h1>Success!</h1>
                 <hr>
                 <p>CSV and PDF report files for books out of stock were sucessfully generated!</p>
                 
             </div>
            
              
            <footer>
                    <img src="images/dummiesSlider.jpg" alt=""/>
            </footer>
            
        </div>    
    </body>
    
</html>
